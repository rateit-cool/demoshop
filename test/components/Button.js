const {Component} = require('./Component');

class Button extends Component {

	constructor(page, selector) {
		super(page, selector);
		this.selector = selector;
	}

	async click() {
		await this.waitFor();
		await this.page.click(this.selector);
	}

}

exports.Button = Button;
