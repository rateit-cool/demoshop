const {Component} = require('./Component');

class TextField extends Component {

	constructor(page, selector) {
		super(page, selector);
		this.selector = selector;
	}

}

exports.TextField = TextField;
