const {Component} = require('./Component');

class InputField extends Component {

	constructor(page, selector) {
		super(page, selector);
		this.selector = selector;
	}

	async type(input) {
		await this.waitFor();
		await this.page.type(this.selector, input);
	}

	async clear() {
		await this.waitFor();
		await this.page.click(this.selector, {clickCount: 3});
		await this.page.keyboard.press('Backspace');
	}
}

exports.InputField = InputField;
