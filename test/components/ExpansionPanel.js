const {Component} = require('./Component');
const constants = require('../constants');

class ExpansionPanel extends Component {

	constructor(page, selector) {
		super(page, selector);
		this.selector = selector;
	}

	async open() {
		await this.waitFor();
		const isExpanded = await this.isExpanded();
		if (!isExpanded){
			await this.page.click(this.selector);
			await this.page.waitFor(constants.TRANSITION_DURATION_ACCORDEON_MILLISECONDS);
		}
	}

	async close() {
		await this.waitFor();
		const isExpanded = await this.isExpanded();
		if (isExpanded){
			await this.page.click(this.selector);
			await this.page.waitFor(constants.TRANSITION_DURATION_ACCORDEON_MILLISECONDS);
		}
	}

	async isExpanded() {
		const cssClasses = await this.getCssClasses();
		return cssClasses.filter(cssClass => cssClass.includes('expanded')).length > 0;
	}

}

exports.ExpansionPanel = ExpansionPanel;
