const {Component} = require('./Component');

class Table extends Component {

	constructor(page, selector) {
		super(page, selector);
		this.selector = selector;
	}

	async getTableRowHtml(index) {
		const row = await page.$eval(`${this.selector} [data-e2e="table-row-${index}"]`, element => element.innerHTML);
		return row;
	}

	async hasTableRowClass(index, className) {
		const row = await page.$$(`${this.selector} .${className}[data-e2e="table-row-${index}"]`);
		return row.length > 0 ? true : false;
	}

	async getNumberOfTableRows() {
		const rows = await page.$$(`${this.selector} [data-e2e^="table-row-"]`);
		return rows.length;
	}

	async clickOnRow(index) {
		await this.waitFor();
		await this.page.click(`${this.selector} [data-e2e="table-row-${index}"]`);
	}
}

exports.Table = Table;
