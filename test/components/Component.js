const logger = console;

class Component {

	constructor(page, selector) {
		this.page = page;
		this.selector = selector;
	}

	async assertContainsText(textToAssert) {
		const text = await this.page.$eval(this.selector, element => element.textContent);

		try {
			await expect(text).toContain(textToAssert);
		} catch (error) {
			logger.error('Selector query:\t\t', this.selector, '\nExpected text to contain:\t', textToAssert, '\nReceived text to contain:\t', text);
			throw error;
		}
	}

	async assertText(textToAssert) {
		await this.waitFor();
		const selectedObject = await this.page.$(this.selector);
		try {
			await expect(selectedObject).toMatch(textToAssert);
		} catch (error) {
			const receivedText = await this.page.$eval(this.selector, element => element.textContent);
			logger.error('Selector query:\t', this.selector, '\nExpected text:\t', textToAssert, '\nReceived text:\t', receivedText);
			throw error;
		}
	}

	async assertComponentIsNotVisible() {
        await this.page.waitForSelector(this.selector, {visible: false});
	}

	async assertComponentIsVisible() {
		await this.waitFor();
		const selectedObject = await this.page.$(this.selector);
        await expect(selectedObject).not.toBe(null);
	}

	async waitFor() {
		await this.page.waitFor(this.selector);
	}

	async getCssClasses() {
		const selectedObject = await this.page.$(this.selector);
		const [,...cssClasses] = selectedObject._remoteObject.description.split('.');
		return cssClasses;
	}
}

exports.Component = Component;
