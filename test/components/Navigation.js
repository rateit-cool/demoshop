const {Component} = require('./Component');
const constants = require('../constants');

class Navigation extends Component {

	constructor(page) {
		super(page);
	}

	async clickOnNavigationItem(pageName) {
		const selector = `a[data-e2e="navigation-drawer-${pageName.split(' ').join('').toLowerCase()}"]`;
		await Promise.all([this.page.waitForNavigation(), this.page.click(selector)]);
		await this.page.waitFor(constants.TRANSITION_DURATION_LEAVINGSCREEN_MILLISECONDS);
	}
}

exports.Navigation = Navigation;
