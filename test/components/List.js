const {Component} = require('./Component');

class List extends Component {

	constructor(page, selector) {
		super(page, selector);
		this.selector = selector;
	}

	async assertComponentIsVisible() {
        await this.page.waitForSelector(this.selector, {visible: true});
	}

	async length() {
		const listOfElements = await this.page.$eval(this.selector, element => element.textContent);
		return listOfElements.trim().length;
	}

}

exports.List = List;
