const {Component} = require('./Component');
const logger = console;

class Tabs extends Component {

    constructor(page, selector) {
        super(page, selector);
        this.selector = selector;
    }

    async contain(...tabs) {
        await this.waitFor();
        const selectedObject = await this.page.$(this.selector);

        const assertions = [];
        for (const tab of tabs) {
            assertions.push(expect(selectedObject).toMatch(tab));
        }
        try {
            await Promise.all(assertions);
        } catch (error) {
            const receivedText = await this.page.$eval(this.selector, element => element.textContent);
            logger.error('Selector query:\t', this.selector, '\nExpected text:\t', tabs, '\nReceived text:\t', receivedText);
            throw error;
        }
    }
}

exports.Tabs = Tabs;
