const {Component} = require('./Component');

class Chart extends Component {

	constructor(page, selector) {
		super(page, selector);
		this.selector = selector;
	}

}

exports.Chart = Chart;
