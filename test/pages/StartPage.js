const {DemoShopPage} = require('./DemoShopPage');
const {Link} = require('../components/Link');
const {TextField} = require('../components/TextField');

class StartPage extends DemoShopPage {

	constructor(page) {
		super(page);
		this.welcomeMessage = new TextField(this.page, 'p.lead');
		this.firstProductLink = new Link(this.page, '.col-sm-4:first-child a');
	}

	async clickOnAuszahlenKachel() {
		await Promise.all([this.page.waitForNavigation(), this.auszahlenKachelButton.click()]);
	}

	async assertWelcomeMessage(textToAssert) {
		await this.welcomeMessage.assertText(textToAssert);
	}

	async assertNumberOfProductElements(value) {
		const liste = await this.page.$$('.thumbnail');
		await expect(liste.length).toEqual(value);
	}

	async assertProductPageWithoutStars() {
		await this.page.waitFor('.rateit-cool-product');
		await expect(await this.page.$$eval('.rateit-cool-product', nodes => nodes.map(n => n.innerText))).toEqual(['','','','','']);
	}

	async clickOnFirstProductLink() {
		await this.firstProductLink.click();
	}

}

exports.StartPage = StartPage;
