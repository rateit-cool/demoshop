const {DemoShopPage} = require('./DemoShopPage');
const {TextField} = require('../components/TextField');
const {List} = require('../components/List');

class ProductDetailPage extends DemoShopPage {

    constructor(page) {
        super(page);
        this.titleText = new TextField(this.page, '.pull-left h4');
        this.starsAtTitleText = new TextField(this.page, '.rateit-cool-product-detail');
        this.listOfFeedbacks = new List(this.page, '.rateit-cool-product-review-list');
    }

    async assertProductDetailTitle(textToAssert) {
        await this.titleText.assertText(textToAssert);
    }

    async assertProductPageWithoutStars() {
		await this.starsAtTitleText.assertText('');
    }
    
    async assertListOfFeedbacksEmpty() {
        expect(await this.listOfFeedbacks.length()).toEqual(0);
    }
}

exports.ProductDetailPage = ProductDetailPage;
