const devices = require('puppeteer/DeviceDescriptors');
const iphone6 = devices['iPhone 6'];
const pixel2 = devices['Pixel 2'];

class DemoShopPage {

	constructor(page) {
		this.page = page;
	}

	async load() {
		await this.page.goto('https://demo.rateit.cool/');
	}

	async setDevice(device) {
		switch (device) {
			case 'iphone':
				await this.page.emulate(iphone6);
				return;
			case 'pixel':
				await this.page.exposeFunction('beforeinstallpromptEvent', () => ({
					prompt: () => ({}),
					userChoice: () => ({})
				}));
				await this.page.emulate(pixel2);
				break;
			default:
				Promise.resolve();
		}
	}

	async reload() {
        await this.page.reload({waitUntil: 'networkidle0'});
	}

	async setAsGoogleBot() {
		await this.page.setUserAgent('Mozilla/5.0 (Linux; Android 6.0.1; Nexus 5X Build/MMB29P) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/41.0.2272.96 Mobile Safari/537.36 (compatible; Googlebot/2.1; +http://www.google.com/bot.html)');
	}

}

exports.DemoShopPage = DemoShopPage;
