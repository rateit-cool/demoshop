const {StartPage} = require('../pages/StartPage');
const {ProductDetailPage} = require('../pages/ProductDetailPage');

describe('DetailPage', () => {

	jest.setTimeout(10000);

	beforeEach(async () => {
		await page.setViewport({width: 1200, height: 2000});
	});

	const startPage = new StartPage(page);
	const productDetailPage = new ProductDetailPage(page);

	it('should call startpage and click on the first product and then show product detail page successfully', async () => {
		await startPage.load();
		await startPage.setAsGoogleBot();

		await startPage.assertWelcomeMessage('Demo Shop rateit.cool');

		await startPage.clickOnFirstProductLink();

		await productDetailPage.assertProductDetailTitle('LOGITECH PerformanceMouse MX Wireless Black910-001120');
	});

	it('should call startpage and click on the first product and then show product detail page successfully and get no stars and no feedbacks', async () => {
		await startPage.load();
		await startPage.setAsGoogleBot();

		await startPage.assertWelcomeMessage('Demo Shop rateit.cool');

		await startPage.clickOnFirstProductLink();

		await productDetailPage.assertProductDetailTitle('LOGITECH PerformanceMouse MX Wireless Black910-001120');
		await productDetailPage.setAsGoogleBot();

		await productDetailPage.assertProductPageWithoutStars();
		await productDetailPage.assertListOfFeedbacksEmpty();
	});

});
