const {StartPage} = require('../pages/StartPage');

describe('user', () => {

	jest.setTimeout(10000);

	beforeEach(async () => {
		await page.setViewport({width: 1200, height: 2000});
	});

	const startPage = new StartPage(page);

	it('should call startpage successfully', async () => {
		await startPage.load();
		await startPage.setAsGoogleBot();

		await startPage.assertWelcomeMessage('Demo Shop rateit.cool');
	});

	it('should call startpage successfully and get 5 product elements, without stars', async () => {
		await startPage.load();
		await startPage.setAsGoogleBot();

		await startPage.assertWelcomeMessage('Demo Shop rateit.cool');

		await startPage.assertNumberOfProductElements(5);

		await startPage.assertProductPageWithoutStars();
	});
});
