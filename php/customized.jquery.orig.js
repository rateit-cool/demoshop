const _customSettings = {
  username: 'YOURUSERNAME',
  apikey: 'YOURAPIKEY',
  limit: 5,
  language: 'DE-de',
  defaultcss: true,
  publicReviews: true,
  securityText: 'demoshop',
  useSecurityText: true,
  labels: LABELS,
  templates: {
    stars: STARS,
    reviews: {
      list: REVIEW_LIST,
      element: REVIEW_ELEMENT,
      overview: REVIEW_OVERVIEW,
      details: REVIEW_DETAILS,
      detailsStars: REVIEW_DETAILSSTARS,
      stars: REVIEW_STARS,
      form: REVIEW_FORM,
      missing: REVIEW_MISSING
    }
  }
};
