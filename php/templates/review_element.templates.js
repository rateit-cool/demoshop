{
    html: '<div class="rateit-cool-review-element"> <hr> <div class=""> <span class="fa $starone"></span> <span class="fa $startwo"></span> <span class="fa $starthree"></span> <span class="fa $starfour"></span> <span class="fa $starfive"></span> <span class="date">$review.time</span> $details <span class="rateit-cool-verified" style="$review.verified_source">$labels.verified_source</span> <span class="rateit-cool-public" style="$review.public_source">$labels.public_source</span> <span class="rateit-cool-mobile" style="$review.mobile_source">$labels.mobile_source</span> <h3>$review.title</h3> <p>$review.content</p> <p class="helpful"> $labels.helpful_label <span class="positive" data-positive="$review.positive" data-language="$review.language" data-gpntype="$review.gpntype" data-gpnvalue="$review.gpnvalue" data-feedbackid="$review.id"> <span class="positiveValue">$review.positive</span> <span class="fa fa-thumbs-o-up"></span> </span>, <span class="negative" data-negative="$review.negative"  data-language="$review.language" data-gpntype="$review.gpntype" data-gpnvalue="$review.gpnvalue" data-feedbackid="$review.id"> <span class="negativeValue">$review.negative</span> <span class="fa fa-thumbs-o-down"></span> </span> </p> <p class="incorrect"> $labels.incorrect_label <span class="report" data-language="$review.language" data-gpntype="$review.gpntype" data-gpnvalue="$review.gpnvalue" data-feedbackid="$review.id"> <span class="fa fa-bullhorn"></span> </span> </p> </div> <div class="clearfix"></div> </div>',
    render: (_inObject, _inDetailLabels) => {
      let starone = 'fa-star-o';
      let startwo = 'fa-star-o';
      let starthree = 'fa-star-o';
      let starfour = 'fa-star-o';
      let starfive = 'fa-star-o';
      if (_inObject.stars >= 1) {
        starone = 'fa-star';
      }
      if (_inObject.stars >= 2) {
        startwo = 'fa-star';
      }
      if (_inObject.stars >= 3) {
        starthree = 'fa-star';
      }
      if (_inObject.stars >= 4) {
        starfour = 'fa-star';
      }
      if (_inObject.stars == 5) {
        starfive = 'fa-star';
      }
      if (!_inObject.details) {
        _inObject.details = {};
      }
      return RateItCoolSettings.templates.reviews.element.html.replace('$review.details.detail1', (_inObject.details.detail1 ? RateItCoolSettings.templates.reviews.stars.starsRender(_inObject.details.detail1, RateItCoolSettings.templates.reviews.detailsStars.html):''))
              .replace('$review.details.detail2', (_inObject.details.detail2 ? RateItCoolSettings.templates.reviews.stars.starsRender(_inObject.details.detail2, RateItCoolSettings.templates.reviews.detailsStars.html):''))
              .replace('$review.details.detail3', (_inObject.details.detail3 ? RateItCoolSettings.templates.reviews.stars.starsRender(_inObject.details.detail3, RateItCoolSettings.templates.reviews.detailsStars.html):''))
              .replace('$review.details.detail4', (_inObject.details.detail4 ? RateItCoolSettings.templates.reviews.stars.starsRender(_inObject.details.detail4, RateItCoolSettings.templates.reviews.detailsStars.html):''))
              .replace('$details.detail1.display', (_inObject.details == undefined || _inObject.details.detail1 == undefined ? 'display:none;':''))
              .replace('$details.detail2.display', (_inObject.details == undefined || _inObject.details.detail2 == undefined ? 'display:none;':''))
              .replace('$details.detail3.display', (_inObject.details == undefined || _inObject.details.detail3 == undefined ? 'display:none;':''))
              .replace('$details.detail4.display', (_inObject.details == undefined || _inObject.details.detail4 == undefined ? 'display:none;':''))
              .replace('$details.detail1.title', (_inDetailLabels && _inDetailLabels.detail1 ? _inDetailLabels.detail1:''))
              .replace('$details.detail2.title', (_inDetailLabels && _inDetailLabels.detail2 ? _inDetailLabels.detail2:''))
              .replace('$details.detail3.title', (_inDetailLabels && _inDetailLabels.detail3 ? _inDetailLabels.detail3:''))
              .replace('$details.detail4.title', (_inDetailLabels && _inDetailLabels.detail4 ? _inDetailLabels.detail4:''))
              .replace('$starone', starone)
              .replace('$startwo', startwo)
              .replace('$starthree', starthree)
              .replace('$starfour', starfour)
              .replace('$starfive', starfive)
              .replace('$details', RateItCoolSettings.templates.reviews.element.details.render(_inObject, _inDetailLabels))
              .replace('$review.title', _inObject.title)
              .replace('$review.content', _inObject.content)
              .split('$review.positive').join(_inObject.positive)
              .split('$review.negative').join(_inObject.negative)
              .split('$review.incorrect').join(_inObject.incorrect)
              .split('$review.id').join(_inObject._id)
              .split('$review.gpntype').join(_inObject.gpntype)
              .split('$review.gpnvalue').join(_inObject.gpnvalue)
              .split('$review.language').join(_inObject.language)
              .split('$review.time').join(new Date(_inObject.time).toLocaleDateString())
              .split('$labels.verified_source').join(RateItCoolSettings.labels.reviews.element.verified_source)
              .split('$labels.public_source').join(RateItCoolSettings.labels.reviews.element.public_source)
              .split('$labels.mobile_source').join(RateItCoolSettings.labels.reviews.element.mobile_source)
              .split('$review.verified_source').join(_inObject.source == 'verified'?'':'display:none')
              .split('$review.public_source').join(_inObject.source == 'public'?'':'display:none')
              .split('$review.mobile_source').join(_inObject.source == 'mobile'?'':'display:none')
              .split('$labels.helpful_label').join(RateItCoolSettings.labels.reviews.element.helpful_label)
              .split('$labels.incorrect_label').join(RateItCoolSettings.labels.reviews.element.incorrect_label)
              .split('$review.detail.show').join( (_inObject.details.detail1?'':'display:none;'));
    },
    details: {
      html: '<div class="rateit-cool-review-detail" style="$review.detail.show"> <table class="rateit-cool-review-detail-table "> <thead> <tr> <th colspan="2">$labels.detailTitle</th> </tr> </thead> <tr style="$details.detail1.display"> <td>$details.detail1.title</td> <td class="stars">$review.detail.detail1</td> </tr> <tr style="$details.detail2.display"> <td>$details.detail2.title</td> <td class="stars">$review.detail.detail2</td> </tr> <tr style="$details.detail3.display"> <td>$details.detail3.title</td> <td class="stars">$review.detail.detail3</td> </tr> <tr style="$details.detail4.display"> <td>$details.detail4.title</td> <td class="stars">$review.detail.detail4</td> </tr> </table> </div>',
      render: (_inObject, _inDetailLabels) => {
        if (!_inObject.details) {
          return '';
        }
        return RateItCoolSettings.templates.reviews.element.details.html
                .split('$labels.detailTitle').join(RateItCoolSettings.labels.reviews.details.detailTitle)
                .split('$details.detail1.display').join((_inObject.details.detail1?'':'display:none'))
                .split('$details.detail1.title').join((_inDetailLabels.detail1?_inObject.details.detail1:''))
                .split('$details.detail2.display').join((_inObject.details.detail2?'':'display:none'))
                .split('$details.detail2.title').join((_inDetailLabels.detail2?_inObject.details.detail2:''))
                .split('$details.detail3.display').join((_inObject.details.detail3?'':'display:none'))
                .split('$details.detail3.title').join((_inDetailLabels.detail3?_inObject.details.detail3:''))
                .split('$details.detail4.display').join((_inObject.details.detail4?'':'display:none'))
                .split('$details.detail4.title').join((_inDetailLabels.detail4?_inObject.details.detail4:''))
                .split('$review.detail.detail1').join((_inObject.details.detail1?RateItCoolSettings.templates.reviews.stars.starsRender(_inObject,
                  RateItCoolSettings.templates.reviews.detailsStars.html):''))
                .split('$review.detail.detail2').join((_inObject.details.detail2?RateItCoolSettings.templates.reviews.stars.starsRender(_inObject,
                  RateItCoolSettings.templates.reviews.detailsStars.html):''))
                .split('$review.detail.detail3').join((_inObject.details.detail3?RateItCoolSettings.templates.reviews.stars.starsRender(_inObject,
                  RateItCoolSettings.templates.reviews.detailsStars.html):''))
                .split('$review.detail.detail4').join((_inObject.details.detail4?RateItCoolSettings.templates.reviews.stars.starsRender(_inObject,
                  RateItCoolSettings.templates.reviews.detailsStars.html):''));
      }
    }
  }