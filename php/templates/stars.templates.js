{
    html: '<div title="$label.review $review.summary/5"><span class="fa $starone"></span><span class="fa $startwo"></span><span class="fa $starthree"></span><span class="fa $starfour"></span><span class="fa $starfive"></span><span class="comment_numbers">($review.total)</span></div>',
    render: (_inObject) => {
      let starone = 'fa-star-o';
      let startwo = 'fa-star-o';
      let starthree = 'fa-star-o';
      let starfour = 'fa-star-o';
      let starfive = 'fa-star-o';
      if (_inObject.summary >= 1) {
        starone = 'fa-star';
      }
      if (_inObject.summary >= 2 && _inObject.summary < 3) {
        startwo = 'fa-star-half-o';
      } else if (_inObject.summary >= 2.5) {
        startwo = 'fa-star';
      }
      if (_inObject.summary >= 3 && _inObject.summary < 4) {
        starthree = 'fa-star-half-o';
      } else if (_inObject.summary >= 3.5) {
        starthree = 'fa-star';
      }
      if (_inObject.summary >= 4 && _inObject.summary < 5) {
        starfour = 'fa-star-half-o';
      } else if (_inObject.summary >= 4) {
        starfour = 'fa-star';
      }
      if (_inObject.summary == 5) {
        starfive = 'fa-star';
      }
      return RateItCoolSettings.templates.stars.html.split('$review.summary').join(Number((_inObject.summary).toFixed(2)))
                .split('$starone').join(starone)
                .split('$startwo').join(startwo)
                .split('$starthree').join(starthree)
                .split('$starfour').join(starfour)
                .split('$starfive').join(starfive)
                .split('$review.total').join(_inObject.total)
                .split('$label.review').join(RateItCoolSettings.labels.stars.review);
    }
  }