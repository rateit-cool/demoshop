{
    html: '<div class="rateit-cool-missing-reviews-elements">$labels.missingtitle<hr/>$reviewForm</div>',
    render: _inObject => {
      return RateItCoolSettings.templates.reviews.missing.html
                  .replace('$labels.missingtitle',RateItCoolSettings.labels.reviews.missing.missingTitle)

      ;
    }
}