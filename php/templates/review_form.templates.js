{
    html: '<div class="rateit-cool-reviewform"><h3>$labels.reviewFormTitle</h3><form name="productDetailReviewform$gpnvalue"> <input type="hidden" name="gpntype" value="$gpntype"/> <input type="hidden" name="gpnvalue" value="$gpnvalue"/> <input type="hidden" name="language" value="$language"/> <table> <tr class="overall"> <td class="label second">$labels.reviewStarsTitle</td> <td> <span class="reviewStars"> <input type="hidden" name="stars" value="0"/> <span class="fa fa-star-o value1" title="$labels.oneStarTitle"></span> <span class="fa fa-star-o value2" title="$labels.twoStarTitle"></span> <span class="fa fa-star-o value3" title="$labels.threeStarTitle"></span> <span class="fa fa-star-o value4" title="$labels.fourStarTitle"></span> <span class="fa fa-star-o value5" title="$labels.fiveStarTitle"></span> </span> </td> </tr> <tr class="reviewDetail1" style="$display.detail1"> <td class="label">$details.detail1</td> <td> <span class="reviewStars"> <input type="hidden" name="stars" value="0"/> <span class="fa fa-star-o value1" title="$labels.oneStarTitle"></span> <span class="fa fa-star-o value2" title="$labels.twoStarTitle"></span> <span class="fa fa-star-o value3" title="$labels.threeStarTitle"></span> <span class="fa fa-star-o value4" title="$labels.fourStarTitle"></span> <span class="fa fa-star-o value5" title="$labels.fiveStarTitle"></span> </span> </td> </tr> <tr class="reviewDetail2" style="$display.detail2"> <td class="label second">$details.detail2</td> <td> <span class="reviewStars"> <input type="hidden" name="stars" value="0"/> <span class="fa fa-star-o value1" title="$labels.oneStarTitle"></span> <span class="fa fa-star-o value2" title="$labels.twoStarTitle"></span> <span class="fa fa-star-o value3" title="$labels.threeStarTitle"></span> <span class="fa fa-star-o value4" title="$labels.fourStarTitle"></span> <span class="fa fa-star-o value5" title="$labels.fiveStarTitle"></span> </span> </td> </tr> <tr class="reviewDetail3" style="$display.detail3"> <td class="label">$details.detail3</td> <td> <span class="reviewStars"> <input type="hidden" name="stars" value="0"/> <span class="fa fa-star-o value1" title="$labels.oneStarTitle"></span> <span class="fa fa-star-o value2" title="$labels.twoStarTitle"></span> <span class="fa fa-star-o value3" title="$labels.threeStarTitle"></span> <span class="fa fa-star-o value4" title="$labels.fourStarTitle"></span> <span class="fa fa-star-o value5" title="$labels.fiveStarTitle"></span> </span> </td> </tr> <tr class="reviewDetail4" style="$display.detail4"> <td class="label second">$details.detail4</td> <td> <span class="reviewStars"> <input type="hidden" name="stars" value="0"/> <span class="fa fa-star-o value1" title="$labels.oneStarTitle"></span> <span class="fa fa-star-o value2" title="$labels.twoStarTitle"></span> <span class="fa fa-star-o value3" title="$labels.threeStarTitle"></span> <span class="fa fa-star-o value4" title="$labels.fourStarTitle"></span> <span class="fa fa-star-o value5" title="$labels.fiveStarTitle"></span> </span> </td> </tr> </table> <br/> <div class="rateit-cool-review-title"> <input type="text" name="reviewTitle" placeholder="$placeholder.title" /> </div> <div class="rateit-cool-review-content"> <textarea name="reviewContent" placeholder="$placeholder.content"></textarea> </div> <div class="rateit-cool-review-recommend"> <input name="recommend" type="checkbox"> $labels.recommend </div> <br/> <div class="rateit-cool-review-security" style="$display.securitytext"> $labels.security.title <b>$securityText</b> <br/> <input name="securityText" type="text" placeholder=""> </div> <div class="rateit-cool-send-review"> <a href="#" data-formname="productDetailReviewform$gpnvalue" class="btn rateit-cool-send-produkt-review">$labels.submit</a><div class="clearfix"></div> </div> </form> <div class="rateit-cool-send-review-success" style="display:none"> $labels.successtext </div> <div class="rateit-cool-send-review-error" style="display:none"> $labels.errortext </div> </div>',
    render: _inObject => {
      if (RateItCoolSettings.publicReviews) {
        return RateItCoolSettings.templates.reviews.form.html
                .split('$gpntype').join(_inObject.gpntype)
                .split('$gpnvalue').join(_inObject.gpnvalue)
                .split('$language').join(_inObject.language)
                .split('$labels.oneStarTitle').join(RateItCoolSettings.labels.reviews.form.oneStarTitle)
                .split('$labels.twoStarTitle').join(RateItCoolSettings.labels.reviews.form.twoStarTitle)
                .split('$labels.threeStarTitle').join(RateItCoolSettings.labels.reviews.form.threeStarTitle)
                .split('$labels.fourStarTitle').join(RateItCoolSettings.labels.reviews.form.fourStarTitle)
                .split('$labels.fiveStarTitle').join(RateItCoolSettings.labels.reviews.form.fiveStarTitle)
                .replace('$display.detail1', (_inObject.overview.labels.detail1?'':'display:none'))
                .replace('$display.detail2', (_inObject.overview.labels.detail2?'':'display:none'))
                .replace('$display.detail3', (_inObject.overview.labels.detail3?'':'display:none'))
                .replace('$display.detail4', (_inObject.overview.labels.detail4?'':'display:none'))
                .replace('$details.detail1', (_inObject.overview.labels.detail1?_inObject.overview.labels.detail1:''))
                .replace('$details.detail2', (_inObject.overview.labels.detail2?_inObject.overview.labels.detail2:''))
                .replace('$details.detail3', (_inObject.overview.labels.detail3?_inObject.overview.labels.detail3:''))
                .replace('$details.detail4', (_inObject.overview.labels.detail4?_inObject.overview.labels.detail4:''))
                .split('$labels.recommend').join(RateItCoolSettings.labels.reviews.form.recommend)
                .split('$placeholder.title').join(RateItCoolSettings.labels.reviews.form.placeholderTitle)
                .split('$placeholder.content').join(RateItCoolSettings.labels.reviews.form.placeholderContent)
                .replace('$securityText', (RateItCoolSettings.securityText !== ''?RateItCoolSettings.securityText:''))
                .replace('$display.securitytext', (RateItCoolSettings.useSecurityText?'':'display:none'))
                .split('$labels.security.title').join(RateItCoolSettings.labels.reviews.form.securityTitle)
                .split('$labels.submit').join(RateItCoolSettings.labels.reviews.form.submit)
                .split('$labels.successtext').join(RateItCoolSettings.labels.reviews.form.successtext)
                .split('$labels.errortext').join(RateItCoolSettings.labels.reviews.form.errortext)
                .split('$labels.reviewFormTitle').join(RateItCoolSettings.labels.reviews.form.reviewFormTitle)
                .split('$labels.reviewStarsTitle').join(RateItCoolSettings.labels.reviews.form.reviewStarsTitle);
        } else {
          return '';
        }
    },
    afterRender: _jQuery => {

      _jQuery('.reviewStars span.value1').on('click', function(e) {
        e.preventDefault();
        _jQuery(this).parent().find('.fa-star').removeClass('fa-star').addClass('fa-star-o');
        _jQuery(this).removeClass('fa-star-o').addClass('fa-star');
        _jQuery(this).parent().find('input[name=stars]').val(1);
        _jQuery(this).parent().find('.star-text').html(_jQuery(this).attr('title'));
      });

      _jQuery('.reviewStars span.value2').on('click', function(e) {
        e.preventDefault();
        _jQuery(this).parent().find('.fa-star').removeClass('fa-star').addClass('fa-star-o');
        _jQuery(this).parent().find('.value1').removeClass('fa-star-o').addClass('fa-star');
        _jQuery(this).removeClass('fa-star-o').addClass('fa-star');
        _jQuery(this).parent().find('input[name=stars]').val(2);
        _jQuery(this).parent().find('.star-text').html(_jQuery(this).attr('title'));
      });

      _jQuery('.reviewStars span.value3').on('click', function(e) {
        e.preventDefault();
        _jQuery(this).parent().find('.fa-star').removeClass('fa-star').addClass('fa-star-o');
        _jQuery(this).parent().find('.value1').removeClass('fa-star-o').addClass('fa-star');
        _jQuery(this).parent().find('.value2').removeClass('fa-star-o').addClass('fa-star');
        _jQuery(this).removeClass('fa-star-o').addClass('fa-star');
        _jQuery(this).parent().find('input[name=stars]').val(3);
        _jQuery(this).parent().find('.star-text').html(_jQuery(this).attr('title'));
      });

      _jQuery('.reviewStars span.value4').on('click', function(e) {
        e.preventDefault();
        _jQuery(this).parent().find('.fa-star').removeClass('fa-star').addClass('fa-star-o');
        _jQuery(this).parent().find('.value1').removeClass('fa-star-o').addClass('fa-star');
        _jQuery(this).parent().find('.value2').removeClass('fa-star-o').addClass('fa-star');
        _jQuery(this).parent().find('.value3').removeClass('fa-star-o').addClass('fa-star');
        _jQuery(this).removeClass('fa-star-o').addClass('fa-star');
        _jQuery(this).parent().find('input[name=stars]').val(4);
        _jQuery(this).parent().find('.star-text').html(_jQuery(this).attr('title'));
      });

      _jQuery('.reviewStars span.value5').on('click', function(e) {
        e.preventDefault();
        _jQuery(this).parent().find('.fa-star').removeClass('fa-star').addClass('fa-star-o');
        _jQuery(this).parent().find('.value1').removeClass('fa-star-o').addClass('fa-star');
        _jQuery(this).parent().find('.value2').removeClass('fa-star-o').addClass('fa-star');
        _jQuery(this).parent().find('.value3').removeClass('fa-star-o').addClass('fa-star');
        _jQuery(this).parent().find('.value4').removeClass('fa-star-o').addClass('fa-star');
        _jQuery(this).removeClass('fa-star-o').addClass('fa-star');
        _jQuery(this).parent().find('input[name=stars]').val(5);
        _jQuery(this).parent().find('.star-text').html(_jQuery(this).attr('title'));
      });

      _jQuery('.rateit-cool-review-element .positive span.fa').on('click', function(e) {
        e.preventDefault();
        let element = _jQuery(this).parent();
        let type = 'positive';
        RateItCoolApi.put({
          type: type,
          gpntype: element.attr('data-gpntype'),
          gpnvalue: element.attr('data-gpnvalue'),
          language: element.attr('data-language'),
          id: element.attr('data-feedbackid')
        }, response => {
          if (response.success && response.success === 'true') {
            element.attr('data-positive', parseInt(element.attr('data-positive')) +1);
            element.find('.positiveValue').html(element.attr('data-positive'));
          }
        });
      });

      _jQuery('.rateit-cool-review-element .negative span.fa').on('click', function(e) {
        e.preventDefault();
        let element = _jQuery(this).parent();
        let type = 'negative';
        RateItCoolApi.put({
          type: type,
          gpntype: element.attr('data-gpntype'),
          gpnvalue: element.attr('data-gpnvalue'),
          language: element.attr('data-language'),
          id: element.attr('data-feedbackid')
        }, response => {
          if (response.success && response.success === 'true') {
            element.attr('data-negative', parseInt(element.attr('data-negative')) +1);
            element.find('.negativeValue').html(element.attr('data-negative'));
          }
        });

      });

      _jQuery('.rateit-cool-review-element .report span.fa').on('click', function(e) {
        e.preventDefault();
        let element = _jQuery(this).parent();
        let type = 'report';
        RateItCoolApi.put({
          type: type,
          gpntype: element.attr('data-gpntype'),
          gpnvalue: element.attr('data-gpnvalue'),
          language: element.attr('data-language'),
          id: element.attr('data-feedbackid')
        }, () => {

        });

      });

      _jQuery('.rateit-cool-send-review a').on('click', e => {
        e.preventDefault();
        let _form = _jQuery('.rateit-cool-reviewform form');
        // get data
        let data = {
          title: _form.find('input[name=reviewTitle]').val(),
          content: _form.find('textarea[name=reviewContent]').val(),
          stars: _form.find('.overall .reviewStars input[name=stars]').val(),
          details: {

          },
          recommend: 0
        };
        let _detail = _form.find('.reviewDetail1');
        let _detailsExists = false;
        if (_detail && _detail.attr('style') == '') {
          data.details.detail1 = _detail.find('.reviewStars input[name=stars]').val();
          _detailsExists = true;
        }
        _detail = _form.find('.reviewDetail2');
        if (_detail && _detail.attr('style') == '') {
          data.details.detail2 = _detail.find('.reviewStars input[name=stars]').val();
          _detailsExists = true;
        }
        _detail = _form.find('.reviewDetail3');
        if (_detail && _detail.attr('style') == '') {
          data.details.detail3 = _detail.find('.reviewStars input[name=stars]').val();
          _detailsExists = true;
        }
        _detail = _form.find('.reviewDetail4');
        if (_detail && _detail.attr('style') == '') {
          data.details.detail4 = _detail.find('.reviewStars input[name=stars]').val();
          _detailsExists = true;
        }
        if (_detailsExists) {
          data.stars = 0;
        }
        let _secTemp = _form.find('input[name=securityText]');
        let _isValid = false;
        if (RateItCoolSettings.useSecurityText &&  _secTemp && RateItCoolSettings.securityText == _secTemp.val()) {
          _isValid = true;
        } else if (!RateItCoolSettings.useSecurityText) {
          _isValid = true;
        }
        if(_form.find('input[name=recommend]:checked').length > 0) {
          data.recommend = 1;
        }
        if (_isValid) {
          RateItCoolApi.post(data, response => {
            if (response.success && response.success === 'true') {
              _form.hide();
              _form.parent().find('.rateit-cool-send-review-error').hide();
              _form.parent().find('.rateit-cool-send-review-success').show();
            } else {
              _form.parent().find('.rateit-cool-send-review-success').hide();
              _form.parent().find('.rateit-cool-send-review-error').show();
            }
          });
        } else {
          // mark all as red
          if (RateItCoolSettings.useSecurityText && RateItCoolSettings.securityText !== _secTemp.val()) {
            _secTemp.addClass('error');
          }
          if (data.title == '') {
            _form.find('input[name=reviewTitle]').addClass('error');
          }
          if (data.content == '') {
            _form.find('textarea[name=reviewContent]').addClass('error');
          }
        }
      });
    }
  }