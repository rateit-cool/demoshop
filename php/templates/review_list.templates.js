{
    html:'<div class="reviewElements"> <hr/> <div class="rateit-cool-review-form" style="display:none;">$reviewForm<hr/></div> <div class="rateit-cool-all-stars"> <table class="rateit-cool-overview-stars"> <thead> <tr> <th colspan="2">$labels.summaryReviewsTitle</th> </tr> </thead> <tr> <td> <span class="fa fa-star"></span><span class="fa fa-star"></span><span class="fa fa-star"></span><span class="fa fa-star"></span><span class="fa fa-star"></span> $five </td> </tr> <tr> <td> <span class="fa fa-star"></span><span class="fa fa-star"></span><span class="fa fa-star"></span><span class="fa fa-star"></span><span class="fa fa-star-o"></span> $four </td> </tr> <tr> <td> <span class="fa fa-star"></span><span class="fa fa-star"></span><span class="fa fa-star"></span><span class="fa fa-star-o"></span><span class="fa fa-star-o"></span> $three </td> </tr> <tr> <td> <span class="fa fa-star"></span><span class="fa fa-star"></span><span class="fa fa-star-o"></span><span class="fa fa-star-o"></span><span class="fa fa-star-o"></span> $two </td> </tr> <tr> <td> <span class="fa fa-star"></span><span class="fa fa-star-o"></span><span class="fa fa-star-o"></span><span class="fa fa-star-o"></span><span class="fa fa-star-o"></span> $one </td> </tr> </table> <table class="rateit-cool-detail-stars" style="$detail.show"> <thead> <tr> <th colspan="2">$labels.detailTitle ($details.total / $overview.total)</th> </tr> </thead> <tr style="$details.detail1.display"> <td>$details.detail1.title</td> <td class="stars">$detail.detail1</td> </tr> <tr style="$details.detail2.display"> <td>$details.detail2.title</td> <td class="stars">$detail.detail2</td> </tr> <tr style="$details.detail3.display"> <td>$details.detail3.title</td> <td class="stars">$detail.detail3</td> </tr> <tr style="$details.detail4.display"> <td>$details.detail4.title</td> <td class="stars">$detail.detail4</td> </tr> </table> <span class="btn rateit-cool-show-form">$labels.showformBtnLabel</span><div class="clearfix"></div> </div> <hr> <div> <div class="rateit-cool-review-recommend-text"> <span style="float:left;">$labels.numberOfRecommend: $recommend%</span> <span style="float:right;"> <select name="reorderReviews"> <option value="time;desc" $selected.sortTimeDescTitle>$labels.sortTimeDescTitle</option> <option value="time;asc" $selected.sortTimeAscTitle>$labels.sortTimeAscTitle</option> <option value="stars;desc" $selected.sortStarsDescTitle>$labels.sortStarsDescTitle</option> <option value="stars;asc" $selected.sortStarsAscTitle>$labels.sortStarsAscTitle</option> <option value="helpful;desc" $selected.sortHelpfulDescTitle>$labels.sortHelpfulDescTitle</option> <option value="helpful;asc" $selected.sortHelpfulAscTitle>$labels.sortHelpfulAscTitle</option> </select> </span> <div class="clearfix"></div> </div> </div> <div id="comment-list"> $list </div> <hr> <div style="float: right;display: $showNewtLink;"> <a href="#" class="showMoreFeedbacks">$labels.showMoreReviewsLinkText</a> </div> </div>',
    render: (_inObject) => {
      let _parameters = RateItCoolApi.parameters();
      let counts = _inObject.total.all;
      if (_inObject.total.language > 0) {
        counts = _inObject.total.language;
      }
      if (_inObject.total.region > 0) {
        counts = _inObject.total.region;
      }
//<a href="#" class="showOnlyStars" data-stars="5" title="$labels.showOnly5StarsCommentHint">
      return RateItCoolSettings.templates.reviews.list.html
              .split('$five').join(_inObject.overview.stars.five > 0 ? '<a href="#" class="showOnlyStars" data-stars="5" title="$labels.showOnly5StarsCommentHint">'+ _inObject.overview.stars.five + '</a>':'0')
              .split('$four').join(_inObject.overview.stars.four > 0 ? '<a href="#" class="showOnlyStars" data-stars="4" title="$labels.showOnly4StarsCommentHint">'+ _inObject.overview.stars.four + '</a>':'0')
              .split('$three').join(_inObject.overview.stars.three > 0 ? '<a href="#" class="showOnlyStars" data-stars="3" title="$labels.showOnly3StarsCommentHint">'+ _inObject.overview.stars.three + '</a>':'0')
              .split('$two').join(_inObject.overview.stars.two > 0 ? '<a href="#" class="showOnlyStars" data-stars="2" title="$labels.showOnly2StarsCommentHint">'+ _inObject.overview.stars.two + '</a>':'0')
              .split('$one').join(_inObject.overview.stars.one > 0 ? '<a href="#" class="showOnlyStars" data-stars="1" title="$labels.showOnly1StarsCommentHint">'+ _inObject.overview.stars.one + '</a>':'0')
              .split('$labels.sortTimeDescTitle').join(RateItCoolSettings.labels.reviews.list.sortTimeDescTitle)
              .split('$labels.sortTimeAscTitle').join(RateItCoolSettings.labels.reviews.list.sortTimeAscTitle)
              .split('$labels.sortStarsDescTitle').join(RateItCoolSettings.labels.reviews.list.sortStarsDescTitle)
              .split('$labels.sortStarsAscTitle').join(RateItCoolSettings.labels.reviews.list.sortStarsAscTitle)
              .split('$labels.sortHelpfulDescTitle').join(RateItCoolSettings.labels.reviews.list.sortHelpfulDescTitle)
              .split('$labels.sortHelpfulAscTitle').join(RateItCoolSettings.labels.reviews.list.sortHelpfulAscTitle)
              .split('$labels.showOnly5StarsCommentHint').join(RateItCoolSettings.labels.reviews.list.showOnly5StarsCommentHint)
              .split('$labels.showOnly4StarsCommentHint').join(RateItCoolSettings.labels.reviews.list.showOnly4StarsCommentHint)
              .split('$labels.showOnly3StarsCommentHint').join(RateItCoolSettings.labels.reviews.list.showOnly3StarsCommentHint)
              .split('$labels.showOnly2StarsCommentHint').join(RateItCoolSettings.labels.reviews.list.showOnly2StarsCommentHint)
              .split('$labels.showOnly1StarsCommentHint').join(RateItCoolSettings.labels.reviews.list.showOnly1StarsCommentHint)
              .split('$selected.sortTimeDescTitle').join((_parameters.sortField == 'time' && _parameters.sort == 'desc'?'selected="selected"':''))
              .split('$selected.sortTimeAscTitle').join((_parameters.sortField == 'time' && _parameters.sort == 'asc'?'selected="selected"':''))
              .split('$selected.sortStarsDescTitle').join((_parameters.sortField == 'stars' && _parameters.sort == 'desc'?'selected="selected"':''))
              .split('$selected.sortStarsAscTitle').join((_parameters.sortField == 'stars' && _parameters.sort == 'asc'?'selected="selected"':''))
              .split('$selected.sortHelpfulDescTitle').join((_parameters.sortField == 'helpful' && _parameters.sort == 'desc'?'selected="selected"':''))
              .split('$selected.sortHelpfulAscTitle').join((_parameters.sortField == 'helpful' && _parameters.sort == 'asc'?'selected="selected"':''))
              .split('$labels.numberOfRecommend').join(RateItCoolSettings.labels.reviews.list.numberOfRecommend)
              .split('$labels.showMoreReviewsLinkText').join(RateItCoolSettings.labels.reviews.list.showMoreReviewsLinkText)
              .split('$labels.showformBtnLabel').join(RateItCoolSettings.labels.reviews.list.showformBtnLabel)
              .split('$labels.summaryReviewsTitle').join(RateItCoolSettings.labels.reviews.list.summaryReviewsTitle)
              .split('$labels.detailTitle').join(RateItCoolSettings.labels.reviews.list.detailTitle)
              .split('$details.total').join(_inObject.overview.details.total)
              .split('$overview.total').join(_inObject.overview.total)
              .split('$detail.show').join(_inObject.overview.labels.detail1?'':'display:none')
              .split('$details.detail1.title').join(_inObject.overview.labels.detail1?_inObject.overview.labels.detail1:'')
              .split('$details.detail2.title').join(_inObject.overview.labels.detail2?_inObject.overview.labels.detail2:'')
              .split('$details.detail3.title').join(_inObject.overview.labels.detail3?_inObject.overview.labels.detail3:'')
              .split('$details.detail4.title').join(_inObject.overview.labels.detail4?_inObject.overview.labels.detail4:'')
              .replace('$details.detail1.display', (_inObject.overview.details == undefined || _inObject.overview.details.detail1 == undefined ? 'display:none;':''))
              .replace('$details.detail2.display', (_inObject.overview.details == undefined || _inObject.overview.details.detail2 == undefined ? 'display:none;':''))
              .replace('$details.detail3.display', (_inObject.overview.details == undefined || _inObject.overview.details.detail3 == undefined ? 'display:none;':''))
              .replace('$details.detail4.display', (_inObject.overview.details == undefined || _inObject.overview.details.detail4 == undefined ? 'display:none;':''))
              .replace('$detail.detail1', RateItCoolSettings.templates.reviews.stars.starsRender(_inObject.overview.details.detail1, RateItCoolSettings.templates.reviews.detailsStars.html))
              .replace('$detail.detail2', RateItCoolSettings.templates.reviews.stars.starsRender(_inObject.overview.details.detail2, RateItCoolSettings.templates.reviews.detailsStars.html))
              .replace('$detail.detail3', RateItCoolSettings.templates.reviews.stars.starsRender(_inObject.overview.details.detail3, RateItCoolSettings.templates.reviews.detailsStars.html))
              .replace('$detail.detail4', RateItCoolSettings.templates.reviews.stars.starsRender(_inObject.overview.details.detail4, RateItCoolSettings.templates.reviews.detailsStars.html))
              .replace('$showNewtLink', (counts > (_parameters.skip + _inObject.elements.length)?'block':'none'))
              .replace('$recommend', Number(((_inObject.overview.recommend / _inObject.overview.total) *100 ).toFixed(0)))
      },
      afterRender: _jQuery => {
        _jQuery('.rateit-cool-overview-stars a.showOnlyStars').on('click', function(e) {
          e.preventDefault();
          let stars = _jQuery(this).attr('data-stars');
          RateItCoolApi.reviews(
            {stars: stars, refresh: true, addreviews: false, skip:0}, 
            (data) => {

          });
        });

        _jQuery('.rateit-cool-show-form').on('click', e => {
          e.preventDefault();
          _jQuery('.rateit-cool-review-form').toggle('bounce');
        });

        _jQuery('.rateit-cool-product-review-list a.showMoreFeedbacks').on('click', e => {
          e.preventDefault();
          RateItCoolApi.reviews(
            {refresh: false, addreviews: true, skip: RateItCoolSettings.limit}, 
            _inObject => {
              let _parameters = RateItCoolApi.parameters();
              let counts = _inObject.total.all;
              if (_inObject.total.language > 0) {
                counts = _inObject.total.language;
              }
              if (_inObject.total.region > 0) {
                counts = _inObject.total.region;
              }

              if ((counts - _parameters.skip) <= RateItCoolSettings.limit) {
                _jQuery('.rateit-cool-product-review-list a.showMoreFeedbacks').hide();
              }
          });
        });

        _jQuery('.rateit-cool-product-review-list select[name=reorderReviews]').on('change', function(ev) {
          ev.preventDefault();

          let sortFieldValue = _jQuery(this).val();
          let sortFieldSortArray = _jQuery(this).val().split(';');
          let sortField = sortFieldSortArray[0];
          let sort = sortFieldSortArray[1];
          let extraParameter = _jQuery('.rateit-cool-product-review-list').attr('data-extraParameter');
          RateItCoolApi.reviews({refresh: true, addreviews: false, sortField: sortField, sort: sort, extraParameter: extraParameter, skip:0},
               data => {
                   _jQuery('.rateit-cool-product-reviews select[name=reorderReviews] option[value="' + sortFieldValue +  '"]').attr('selected','selected');
               }
          );

        });

      }
  }