<?php

$labels = file_get_contents('customizedLabels.json');

$review_details = file_get_contents('./templates/review_details.templates.js');
$review_form = file_get_contents('./templates/review_form.templates.js');          
$review_overview = file_get_contents('./templates/review_overview.templates.js');
$review_detailsstars = file_get_contents('./templates/review_detailsstars.templates.js');
$review_list = file_get_contents('./templates/review_list.templates.js');
$review_stars = file_get_contents('./templates/review_stars.templates.js');
$review_element = file_get_contents('./templates/review_element.templates.js');
$review_missing = file_get_contents('./templates/review_missing.templates.js');
$stars = file_get_contents('./templates/stars.templates.js');

$customizedJquery = file_get_contents('./customized.jquery.orig.js');

$destinationJs = str_replace('LABELS', $labels, $customizedJquery);
$destinationJs = str_replace('REVIEW_FORM', $review_form, $destinationJs);
$destinationJs = str_replace('REVIEW_OVERVIEW', $review_overview, $destinationJs);
$destinationJs = str_replace('REVIEW_DETAILSSTARS', $review_detailsstars, $destinationJs);
$destinationJs = str_replace('REVIEW_DETAILS', $review_details, $destinationJs);
$destinationJs = str_replace('REVIEW_LIST', $review_list, $destinationJs);
$destinationJs = str_replace('REVIEW_STARS', $review_stars, $destinationJs);
$destinationJs = str_replace('REVIEW_ELEMENT', $review_element, $destinationJs);
$destinationJs = str_replace('REVIEW_MISSING', $review_missing, $destinationJs);
$destinationJs = str_replace('STARS', $stars, $destinationJs);

$jqueryOrigFile = file_get_contents('./rateit.jquery.orig.js');
$completeJqueryJs = str_replace('RATEITCOOLSETTINGS', $destinationJs, $jqueryOrigFile);

file_put_contents('./rateit.jquery.js', $completeJqueryJs);
