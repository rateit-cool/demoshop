const RateItCoolSettings = {
  username: 'YOURUSERNAME',
  apikey: 'YOURAPIKEY',
  limit: 5,
  language: 'DE-de',
  defaultcss: true,
  publicReviews: true,
  securityText: 'demoshop',
  useSecurityText: true,
  labels: {  
  "reviews":{  
     "details":{  
        "detailTitle":"Review in detail"
     },
     "element":{  
        "verified_source":"Verified buyer",
        "public_source":"Public review",
        "mobile_source":"App review",
        "helpful_label":"Customer thinks this is helfpul",
        "incorrect_label":"Report the review"
     },
     "form":{  
        "reviewStarsTitle":"Overview",
        "oneStarTitle":"not realy ok",
        "twoStarTitle":"hm ok",
        "threeStarTitle":"ok",
        "fourStarTitle":"cool",
        "fiveStarTitle":"very cool",
        "recommend":"I recommend this product",
        "placeholderTitle":"Please enter a review title",
        "placeholderContent":"Please write some content of the review",
        "securityTitle":"Security text",
        "submit":"Send the review",
        "successtext":"Thank you for the review",
        "errortext":"There is an error while sending the review.",
        "reviewFormTitle":"Review the product"
     },
     "list":{  
        "showOnly5StarsCommentHint":"Show only reviews with 5 stars",
        "showOnly4StarsCommentHint":"Show only reviews with 4 stars",
        "showOnly3StarsCommentHint":"Show only reviews with 3 stars",
        "showOnly2StarsCommentHint":"Show only reviews with 2 stars",
        "showOnly1StarsCommentHint":"Show only reviews with 1 star",
        "numberOfRecommend":"Customers recommend this product",
        "sortTimeDescTitle":"Show the newest review first",
        "sortTimeAscTitle":"Show the oldest review first",
        "sortStarsDescTitle":"Show best reviews first",
        "sortStarsAscTitle":"Show worst review first",
        "sortHelpfulDescTitle":"Show the helpful review first",
        "sortHelpfulAscTitle":"Show the worst helpful review first",
        "showMoreReviewsLinkText":"show more reviews...",
        "summaryReviewsTitle":"Reviews",
        "detailTitle":"Reviews in detail",
        "showformBtnLabel":"Write your own review"
     },
     "missing":{  
        "missingTitle":"Sorry, but don\"t have any review for this roduct yet. Be the first to write a review."
     },
     "overview":{  
        "summaryReviewsTitle":"Overview"
     },
     "stars":{  
        "review":"Reviews",
        "summaryReviewsTitle":"Overview"
     }
  },
  "stars":{  
     "review":"Reviews"
  }
},
  templates: {
    stars: {
    html: '<div title="$label.review $review.summary/5"><span class="fa $starone"></span><span class="fa $startwo"></span><span class="fa $starthree"></span><span class="fa $starfour"></span><span class="fa $starfive"></span><span class="comment_numbers">($review.total)</span></div>',
    render: (_inObject) => {
      let starone = 'fa-star-o';
      let startwo = 'fa-star-o';
      let starthree = 'fa-star-o';
      let starfour = 'fa-star-o';
      let starfive = 'fa-star-o';
      if (_inObject.summary >= 1) {
        starone = 'fa-star';
      }
      if (_inObject.summary >= 2 && _inObject.summary < 3) {
        startwo = 'fa-star-half-o';
      } else if (_inObject.summary >= 2.5) {
        startwo = 'fa-star';
      }
      if (_inObject.summary >= 3 && _inObject.summary < 4) {
        starthree = 'fa-star-half-o';
      } else if (_inObject.summary >= 3.5) {
        starthree = 'fa-star';
      }
      if (_inObject.summary >= 4 && _inObject.summary < 5) {
        starfour = 'fa-star-half-o';
      } else if (_inObject.summary >= 4) {
        starfour = 'fa-star';
      }
      if (_inObject.summary == 5) {
        starfive = 'fa-star';
      }
      return RateItCoolSettings.templates.stars.html.split('$review.summary').join(Number((_inObject.summary).toFixed(2)))
                .split('$starone').join(starone)
                .split('$startwo').join(startwo)
                .split('$starthree').join(starthree)
                .split('$starfour').join(starfour)
                .split('$starfive').join(starfive)
                .split('$review.total').join(_inObject.total)
                .split('$label.review').join(RateItCoolSettings.labels.stars.review);
    }
  },
    reviews: {
      list: {
    html:'<div class="reviewElements"> <hr/> <div class="rateit-cool-review-form" style="display:none;">$reviewForm<hr/></div> <div class="rateit-cool-all-stars"> <table class="rateit-cool-overview-stars"> <thead> <tr> <th colspan="2">$labels.summaryReviewsTitle</th> </tr> </thead> <tr> <td> <span class="fa fa-star"></span><span class="fa fa-star"></span><span class="fa fa-star"></span><span class="fa fa-star"></span><span class="fa fa-star"></span> $five </td> </tr> <tr> <td> <span class="fa fa-star"></span><span class="fa fa-star"></span><span class="fa fa-star"></span><span class="fa fa-star"></span><span class="fa fa-star-o"></span> $four </td> </tr> <tr> <td> <span class="fa fa-star"></span><span class="fa fa-star"></span><span class="fa fa-star"></span><span class="fa fa-star-o"></span><span class="fa fa-star-o"></span> $three </td> </tr> <tr> <td> <span class="fa fa-star"></span><span class="fa fa-star"></span><span class="fa fa-star-o"></span><span class="fa fa-star-o"></span><span class="fa fa-star-o"></span> $two </td> </tr> <tr> <td> <span class="fa fa-star"></span><span class="fa fa-star-o"></span><span class="fa fa-star-o"></span><span class="fa fa-star-o"></span><span class="fa fa-star-o"></span> $one </td> </tr> </table> <table class="rateit-cool-detail-stars" style="$detail.show"> <thead> <tr> <th colspan="2">$labels.detailTitle ($details.total / $overview.total)</th> </tr> </thead> <tr style="$details.detail1.display"> <td>$details.detail1.title</td> <td class="stars">$detail.detail1</td> </tr> <tr style="$details.detail2.display"> <td>$details.detail2.title</td> <td class="stars">$detail.detail2</td> </tr> <tr style="$details.detail3.display"> <td>$details.detail3.title</td> <td class="stars">$detail.detail3</td> </tr> <tr style="$details.detail4.display"> <td>$details.detail4.title</td> <td class="stars">$detail.detail4</td> </tr> </table> <span class="btn rateit-cool-show-form">$labels.showformBtnLabel</span><div class="clearfix"></div> </div> <hr> <div> <div class="rateit-cool-review-recommend-text"> <span style="float:left;">$labels.numberOfRecommend: $recommend%</span> <span style="float:right;"> <select name="reorderReviews"> <option value="time;desc" $selected.sortTimeDescTitle>$labels.sortTimeDescTitle</option> <option value="time;asc" $selected.sortTimeAscTitle>$labels.sortTimeAscTitle</option> <option value="stars;desc" $selected.sortStarsDescTitle>$labels.sortStarsDescTitle</option> <option value="stars;asc" $selected.sortStarsAscTitle>$labels.sortStarsAscTitle</option> <option value="helpful;desc" $selected.sortHelpfulDescTitle>$labels.sortHelpfulDescTitle</option> <option value="helpful;asc" $selected.sortHelpfulAscTitle>$labels.sortHelpfulAscTitle</option> </select> </span> <div class="clearfix"></div> </div> </div> <div id="comment-list"> $list </div> <hr> <div style="float: right;display: $showNewtLink;"> <a href="#" class="showMoreFeedbacks">$labels.showMoreReviewsLinkText</a> </div> </div>',
    render: (_inObject) => {
      let _parameters = RateItCoolApi.parameters();
      let counts = _inObject.total.all;
      if (_inObject.total.language > 0) {
        counts = _inObject.total.language;
      }
      if (_inObject.total.region > 0) {
        counts = _inObject.total.region;
      }
//<a href="#" class="showOnlyStars" data-stars="5" title="$labels.showOnly5StarsCommentHint">
      return RateItCoolSettings.templates.reviews.list.html
              .split('$five').join(_inObject.overview.stars.five > 0 ? '<a href="#" class="showOnlyStars" data-stars="5" title="$labels.showOnly5StarsCommentHint">'+ _inObject.overview.stars.five + '</a>':'0')
              .split('$four').join(_inObject.overview.stars.four > 0 ? '<a href="#" class="showOnlyStars" data-stars="4" title="$labels.showOnly4StarsCommentHint">'+ _inObject.overview.stars.four + '</a>':'0')
              .split('$three').join(_inObject.overview.stars.three > 0 ? '<a href="#" class="showOnlyStars" data-stars="3" title="$labels.showOnly3StarsCommentHint">'+ _inObject.overview.stars.three + '</a>':'0')
              .split('$two').join(_inObject.overview.stars.two > 0 ? '<a href="#" class="showOnlyStars" data-stars="2" title="$labels.showOnly2StarsCommentHint">'+ _inObject.overview.stars.two + '</a>':'0')
              .split('$one').join(_inObject.overview.stars.one > 0 ? '<a href="#" class="showOnlyStars" data-stars="1" title="$labels.showOnly1StarsCommentHint">'+ _inObject.overview.stars.one + '</a>':'0')
              .split('$labels.sortTimeDescTitle').join(RateItCoolSettings.labels.reviews.list.sortTimeDescTitle)
              .split('$labels.sortTimeAscTitle').join(RateItCoolSettings.labels.reviews.list.sortTimeAscTitle)
              .split('$labels.sortStarsDescTitle').join(RateItCoolSettings.labels.reviews.list.sortStarsDescTitle)
              .split('$labels.sortStarsAscTitle').join(RateItCoolSettings.labels.reviews.list.sortStarsAscTitle)
              .split('$labels.sortHelpfulDescTitle').join(RateItCoolSettings.labels.reviews.list.sortHelpfulDescTitle)
              .split('$labels.sortHelpfulAscTitle').join(RateItCoolSettings.labels.reviews.list.sortHelpfulAscTitle)
              .split('$labels.showOnly5StarsCommentHint').join(RateItCoolSettings.labels.reviews.list.showOnly5StarsCommentHint)
              .split('$labels.showOnly4StarsCommentHint').join(RateItCoolSettings.labels.reviews.list.showOnly4StarsCommentHint)
              .split('$labels.showOnly3StarsCommentHint').join(RateItCoolSettings.labels.reviews.list.showOnly3StarsCommentHint)
              .split('$labels.showOnly2StarsCommentHint').join(RateItCoolSettings.labels.reviews.list.showOnly2StarsCommentHint)
              .split('$labels.showOnly1StarsCommentHint').join(RateItCoolSettings.labels.reviews.list.showOnly1StarsCommentHint)
              .split('$selected.sortTimeDescTitle').join((_parameters.sortField == 'time' && _parameters.sort == 'desc'?'selected="selected"':''))
              .split('$selected.sortTimeAscTitle').join((_parameters.sortField == 'time' && _parameters.sort == 'asc'?'selected="selected"':''))
              .split('$selected.sortStarsDescTitle').join((_parameters.sortField == 'stars' && _parameters.sort == 'desc'?'selected="selected"':''))
              .split('$selected.sortStarsAscTitle').join((_parameters.sortField == 'stars' && _parameters.sort == 'asc'?'selected="selected"':''))
              .split('$selected.sortHelpfulDescTitle').join((_parameters.sortField == 'helpful' && _parameters.sort == 'desc'?'selected="selected"':''))
              .split('$selected.sortHelpfulAscTitle').join((_parameters.sortField == 'helpful' && _parameters.sort == 'asc'?'selected="selected"':''))
              .split('$labels.numberOfRecommend').join(RateItCoolSettings.labels.reviews.list.numberOfRecommend)
              .split('$labels.showMoreReviewsLinkText').join(RateItCoolSettings.labels.reviews.list.showMoreReviewsLinkText)
              .split('$labels.showformBtnLabel').join(RateItCoolSettings.labels.reviews.list.showformBtnLabel)
              .split('$labels.summaryReviewsTitle').join(RateItCoolSettings.labels.reviews.list.summaryReviewsTitle)
              .split('$labels.detailTitle').join(RateItCoolSettings.labels.reviews.list.detailTitle)
              .split('$details.total').join(_inObject.overview.details.total)
              .split('$overview.total').join(_inObject.overview.total)
              .split('$detail.show').join(_inObject.overview.labels.detail1?'':'display:none')
              .split('$details.detail1.title').join(_inObject.overview.labels.detail1?_inObject.overview.labels.detail1:'')
              .split('$details.detail2.title').join(_inObject.overview.labels.detail2?_inObject.overview.labels.detail2:'')
              .split('$details.detail3.title').join(_inObject.overview.labels.detail3?_inObject.overview.labels.detail3:'')
              .split('$details.detail4.title').join(_inObject.overview.labels.detail4?_inObject.overview.labels.detail4:'')
              .replace('$details.detail1.display', (_inObject.overview.details == undefined || _inObject.overview.details.detail1 == undefined ? 'display:none;':''))
              .replace('$details.detail2.display', (_inObject.overview.details == undefined || _inObject.overview.details.detail2 == undefined ? 'display:none;':''))
              .replace('$details.detail3.display', (_inObject.overview.details == undefined || _inObject.overview.details.detail3 == undefined ? 'display:none;':''))
              .replace('$details.detail4.display', (_inObject.overview.details == undefined || _inObject.overview.details.detail4 == undefined ? 'display:none;':''))
              .replace('$detail.detail1', RateItCoolSettings.templates.reviews.stars.starsRender(_inObject.overview.details.detail1, RateItCoolSettings.templates.reviews.detailsStars.html))
              .replace('$detail.detail2', RateItCoolSettings.templates.reviews.stars.starsRender(_inObject.overview.details.detail2, RateItCoolSettings.templates.reviews.detailsStars.html))
              .replace('$detail.detail3', RateItCoolSettings.templates.reviews.stars.starsRender(_inObject.overview.details.detail3, RateItCoolSettings.templates.reviews.detailsStars.html))
              .replace('$detail.detail4', RateItCoolSettings.templates.reviews.stars.starsRender(_inObject.overview.details.detail4, RateItCoolSettings.templates.reviews.detailsStars.html))
              .replace('$showNewtLink', (counts > (_parameters.skip + _inObject.elements.length)?'block':'none'))
              .replace('$recommend', Number(((_inObject.overview.recommend / _inObject.overview.total) *100 ).toFixed(0)))
      },
      afterRender: _jQuery => {
        _jQuery('.rateit-cool-overview-stars a.showOnlyStars').on('click', function(e) {
          e.preventDefault();
          let stars = _jQuery(this).attr('data-stars');
          RateItCoolApi.reviews(
            {stars: stars, refresh: true, addreviews: false, skip:0}, 
            (data) => {

          });
        });

        _jQuery('.rateit-cool-show-form').on('click', e => {
          e.preventDefault();
          _jQuery('.rateit-cool-review-form').toggle('bounce');
        });

        _jQuery('.rateit-cool-product-review-list a.showMoreFeedbacks').on('click', e => {
          e.preventDefault();
          RateItCoolApi.reviews(
            {refresh: false, addreviews: true, skip: RateItCoolSettings.limit}, 
            _inObject => {
              let _parameters = RateItCoolApi.parameters();
              let counts = _inObject.total.all;
              if (_inObject.total.language > 0) {
                counts = _inObject.total.language;
              }
              if (_inObject.total.region > 0) {
                counts = _inObject.total.region;
              }

              if ((counts - _parameters.skip) <= RateItCoolSettings.limit) {
                _jQuery('.rateit-cool-product-review-list a.showMoreFeedbacks').hide();
              }
          });
        });

        _jQuery('.rateit-cool-product-review-list select[name=reorderReviews]').on('change', function(ev) {
          ev.preventDefault();

          let sortFieldValue = _jQuery(this).val();
          let sortFieldSortArray = _jQuery(this).val().split(';');
          let sortField = sortFieldSortArray[0];
          let sort = sortFieldSortArray[1];
          let extraParameter = _jQuery('.rateit-cool-product-review-list').attr('data-extraParameter');
          RateItCoolApi.reviews({refresh: true, addreviews: false, sortField: sortField, sort: sort, extraParameter: extraParameter, skip:0},
               data => {
                   _jQuery('.rateit-cool-product-reviews select[name=reorderReviews] option[value="' + sortFieldValue +  '"]').attr('selected','selected');
               }
          );

        });

      }
  },
      element: {
    html: '<div class="rateit-cool-review-element"> <hr> <div class=""> <span class="fa $starone"></span> <span class="fa $startwo"></span> <span class="fa $starthree"></span> <span class="fa $starfour"></span> <span class="fa $starfive"></span> <span class="date">$review.time</span> $details <span class="rateit-cool-verified" style="$review.verified_source">$labels.verified_source</span> <span class="rateit-cool-public" style="$review.public_source">$labels.public_source</span> <span class="rateit-cool-mobile" style="$review.mobile_source">$labels.mobile_source</span> <h3>$review.title</h3> <p>$review.content</p> <p class="helpful"> $labels.helpful_label <span class="positive" data-positive="$review.positive" data-language="$review.language" data-gpntype="$review.gpntype" data-gpnvalue="$review.gpnvalue" data-feedbackid="$review.id"> <span class="positiveValue">$review.positive</span> <span class="fa fa-thumbs-o-up"></span> </span>, <span class="negative" data-negative="$review.negative"  data-language="$review.language" data-gpntype="$review.gpntype" data-gpnvalue="$review.gpnvalue" data-feedbackid="$review.id"> <span class="negativeValue">$review.negative</span> <span class="fa fa-thumbs-o-down"></span> </span> </p> <p class="incorrect"> $labels.incorrect_label <span class="report" data-language="$review.language" data-gpntype="$review.gpntype" data-gpnvalue="$review.gpnvalue" data-feedbackid="$review.id"> <span class="fa fa-bullhorn"></span> </span> </p> </div> <div class="clearfix"></div> </div>',
    render: (_inObject, _inDetailLabels) => {
      let starone = 'fa-star-o';
      let startwo = 'fa-star-o';
      let starthree = 'fa-star-o';
      let starfour = 'fa-star-o';
      let starfive = 'fa-star-o';
      if (_inObject.stars >= 1) {
        starone = 'fa-star';
      }
      if (_inObject.stars >= 2) {
        startwo = 'fa-star';
      }
      if (_inObject.stars >= 3) {
        starthree = 'fa-star';
      }
      if (_inObject.stars >= 4) {
        starfour = 'fa-star';
      }
      if (_inObject.stars == 5) {
        starfive = 'fa-star';
      }
      if (!_inObject.details) {
        _inObject.details = {};
      }
      return RateItCoolSettings.templates.reviews.element.html.replace('$review.details.detail1', (_inObject.details.detail1 ? RateItCoolSettings.templates.reviews.stars.starsRender(_inObject.details.detail1, RateItCoolSettings.templates.reviews.detailsStars.html):''))
              .replace('$review.details.detail2', (_inObject.details.detail2 ? RateItCoolSettings.templates.reviews.stars.starsRender(_inObject.details.detail2, RateItCoolSettings.templates.reviews.detailsStars.html):''))
              .replace('$review.details.detail3', (_inObject.details.detail3 ? RateItCoolSettings.templates.reviews.stars.starsRender(_inObject.details.detail3, RateItCoolSettings.templates.reviews.detailsStars.html):''))
              .replace('$review.details.detail4', (_inObject.details.detail4 ? RateItCoolSettings.templates.reviews.stars.starsRender(_inObject.details.detail4, RateItCoolSettings.templates.reviews.detailsStars.html):''))
              .replace('$details.detail1.display', (_inObject.details == undefined || _inObject.details.detail1 == undefined ? 'display:none;':''))
              .replace('$details.detail2.display', (_inObject.details == undefined || _inObject.details.detail2 == undefined ? 'display:none;':''))
              .replace('$details.detail3.display', (_inObject.details == undefined || _inObject.details.detail3 == undefined ? 'display:none;':''))
              .replace('$details.detail4.display', (_inObject.details == undefined || _inObject.details.detail4 == undefined ? 'display:none;':''))
              .replace('$details.detail1.title', (_inDetailLabels && _inDetailLabels.detail1 ? _inDetailLabels.detail1:''))
              .replace('$details.detail2.title', (_inDetailLabels && _inDetailLabels.detail2 ? _inDetailLabels.detail2:''))
              .replace('$details.detail3.title', (_inDetailLabels && _inDetailLabels.detail3 ? _inDetailLabels.detail3:''))
              .replace('$details.detail4.title', (_inDetailLabels && _inDetailLabels.detail4 ? _inDetailLabels.detail4:''))
              .replace('$starone', starone)
              .replace('$startwo', startwo)
              .replace('$starthree', starthree)
              .replace('$starfour', starfour)
              .replace('$starfive', starfive)
              .replace('$details', RateItCoolSettings.templates.reviews.element.details.render(_inObject, _inDetailLabels))
              .replace('$review.title', _inObject.title)
              .replace('$review.content', _inObject.content)
              .split('$review.positive').join(_inObject.positive)
              .split('$review.negative').join(_inObject.negative)
              .split('$review.incorrect').join(_inObject.incorrect)
              .split('$review.id').join(_inObject._id)
              .split('$review.gpntype').join(_inObject.gpntype)
              .split('$review.gpnvalue').join(_inObject.gpnvalue)
              .split('$review.language').join(_inObject.language)
              .split('$review.time').join(new Date(_inObject.time).toLocaleDateString())
              .split('$labels.verified_source').join(RateItCoolSettings.labels.reviews.element.verified_source)
              .split('$labels.public_source').join(RateItCoolSettings.labels.reviews.element.public_source)
              .split('$labels.mobile_source').join(RateItCoolSettings.labels.reviews.element.mobile_source)
              .split('$review.verified_source').join(_inObject.source == 'verified'?'':'display:none')
              .split('$review.public_source').join(_inObject.source == 'public'?'':'display:none')
              .split('$review.mobile_source').join(_inObject.source == 'mobile'?'':'display:none')
              .split('$labels.helpful_label').join(RateItCoolSettings.labels.reviews.element.helpful_label)
              .split('$labels.incorrect_label').join(RateItCoolSettings.labels.reviews.element.incorrect_label)
              .split('$review.detail.show').join( (_inObject.details.detail1?'':'display:none;'));
    },
    details: {
      html: '<div class="rateit-cool-review-detail" style="$review.detail.show"> <table class="rateit-cool-review-detail-table "> <thead> <tr> <th colspan="2">$labels.detailTitle</th> </tr> </thead> <tr style="$details.detail1.display"> <td>$details.detail1.title</td> <td class="stars">$review.detail.detail1</td> </tr> <tr style="$details.detail2.display"> <td>$details.detail2.title</td> <td class="stars">$review.detail.detail2</td> </tr> <tr style="$details.detail3.display"> <td>$details.detail3.title</td> <td class="stars">$review.detail.detail3</td> </tr> <tr style="$details.detail4.display"> <td>$details.detail4.title</td> <td class="stars">$review.detail.detail4</td> </tr> </table> </div>',
      render: (_inObject, _inDetailLabels) => {
        if (!_inObject.details) {
          return '';
        }
        return RateItCoolSettings.templates.reviews.element.details.html
                .split('$labels.detailTitle').join(RateItCoolSettings.labels.reviews.details.detailTitle)
                .split('$details.detail1.display').join((_inObject.details.detail1?'':'display:none'))
                .split('$details.detail1.title').join((_inDetailLabels.detail1?_inObject.details.detail1:''))
                .split('$details.detail2.display').join((_inObject.details.detail2?'':'display:none'))
                .split('$details.detail2.title').join((_inDetailLabels.detail2?_inObject.details.detail2:''))
                .split('$details.detail3.display').join((_inObject.details.detail3?'':'display:none'))
                .split('$details.detail3.title').join((_inDetailLabels.detail3?_inObject.details.detail3:''))
                .split('$details.detail4.display').join((_inObject.details.detail4?'':'display:none'))
                .split('$details.detail4.title').join((_inDetailLabels.detail4?_inObject.details.detail4:''))
                .split('$review.detail.detail1').join((_inObject.details.detail1?RateItCoolSettings.templates.reviews.stars.starsRender(_inObject,
                  RateItCoolSettings.templates.reviews.detailsStars.html):''))
                .split('$review.detail.detail2').join((_inObject.details.detail2?RateItCoolSettings.templates.reviews.stars.starsRender(_inObject,
                  RateItCoolSettings.templates.reviews.detailsStars.html):''))
                .split('$review.detail.detail3').join((_inObject.details.detail3?RateItCoolSettings.templates.reviews.stars.starsRender(_inObject,
                  RateItCoolSettings.templates.reviews.detailsStars.html):''))
                .split('$review.detail.detail4').join((_inObject.details.detail4?RateItCoolSettings.templates.reviews.stars.starsRender(_inObject,
                  RateItCoolSettings.templates.reviews.detailsStars.html):''));
      }
    }
  },
      overview: {
    html: '<div class="rateit-cool-stars-detail-table" style="display: none;"><table><tbody><tr><th colspan="2">$label.summaryReviewsTitle</th></tr></tbody><tr> <td class="stars"><span class="fa fa-star"></span><span class="fa fa-star"></span><span class="fa fa-star"></span><span class="fa fa-star"></span><span class="fa fa-star"></span></td> <td class="starValue">$review.star5</td> </tr> <tr> <td class="stars"><span class="fa fa-star"></span><span class="fa fa-star"></span><span class="fa fa-star"></span><span class="fa fa-star"></span><span class="fa fa-star-o"></span></td> <td class="starValue">$review.star4</td> </tr> <tr> <td class="stars"><span class="fa fa-star"></span><span class="fa fa-star"></span><span class="fa fa-star"></span><span class="fa fa-star-o"></span><span class="fa fa-star-o"></span></td> <td class="starValue">$review.star3</td> </tr> <tr> <td class="stars"><span class="fa fa-star"></span><span class="fa fa-star"></span><span class="fa fa-star-o"></span><span class="fa fa-star-o"></span><span class="fa fa-star-o"></span></span></td> <td class="starValue">$review.star2</td> </tr> <tr> <td class="stars"><span class="fa fa-star"></span><span class="fa fa-star-o"></span><span class="fa fa-star-o"></span><span class="fa fa-star-o"></span><span class="fa fa-star-o"></span></td> <td class="starValue">$review.star1</td> </tr> </table> $detailsTable </div> </div>',
    labels: {
      summaryReviewsTitle: 'Übersicht'
    }
  },
      details: {
    html: ' <table> <tbody> <tr> <th colspan="2">$label.detailTitle ($details.total/$review.total)</th> </tr> </tbody> <tr style="$details.detail1.display"> <td>$details.detail1.title</td> <td class="stars">$review.details.detail1</td> </tr> <tr style="$details.detail2.display"> <td>$details.detail2.title</td> <td class="stars">$review.details.detail2</td> </tr> <tr style="$details.detail3.display"> <td>$details.detail3.title</td> <td class="stars">$review.details.detail3</td> </tr> <tr style="$details.detail4.display"> <td>$details.detail4.title</td> <td class="stars">$review.details.detail4</td> </tr> </table>'
},
      detailsStars: {
    html: '<span class="fa $starone"></span><span class="fa $startwo"></span><span class="fa $starthree"></span><span class="fa $starfour"></span><span class="fa $starfive"></span>'
},
      stars: {
    html: '<div title="$label.review $review.summary/5"><span class="fa $starone"></span><span class="fa $startwo"></span><span class="fa $starthree"></span><span class="fa $starfour"></span><span class="fa $starfive"></span><span class="comment_numbers">($review.total)</span> <span class="fa fa-bars rateit-cool-show-stars"></span></div>',
    starsRender: (_inValue, template) => {
      let starone = 'fa-star-o';
      let startwo = 'fa-star-o';
      let starthree = 'fa-star-o';
      let starfour = 'fa-star-o';
      let starfive = 'fa-star-o';
      if (_inValue >= 1) {
        starone = 'fa-star';
      }
      if (_inValue >= 2 && _inValue < 3) {
        startwo = 'fa-star-half-o';
      } else if (_inValue >= 2) {
        startwo = 'fa-star';
      }
      if (_inValue >= 3 && _inValue < 4) {
        starthree = 'fa-star-half-o';
      } else if (_inValue >= 3) {
        starthree = 'fa-star';
      }
      if (_inValue >= 4 && _inValue < 5) {
        starfour = 'fa-star-half-o';
      } else if (_inValue >= 4) {
        starfour = 'fa-star';
      }
      if (_inValue == 5) {
        starfive = 'fa-star';
      }
      return template.split('$starone').join(starone)
                    .split('$startwo').join(startwo)
                    .split('$starthree').join(starthree)
                    .split('$starfour').join(starfour)
                    .split('$starfive').join(starfive);
    },
    render: _inObject => {
      let starone = 'fa-star-o';
      let startwo = 'fa-star-o';
      let starthree = 'fa-star-o';
      let starfour = 'fa-star-o';
      let starfive = 'fa-star-o';
      if (_inObject.summary >= 1) {
        starone = 'fa-star';
      }
      if (_inObject.summary >= 2 && _inObject.summary < 3) {
        startwo = 'fa-star-half-o';
      } else if (_inObject.summary >= 2) {
        startwo = 'fa-star';
      }
      if (_inObject.summary >= 3 && _inObject.summary < 4) {
        starthree = 'fa-star-half-o';
      } else if (_inObject.summary >= 3) {
        starthree = 'fa-star';
      }
      if (_inObject.summary >= 4 && _inObject.summary < 5) {
        starfour = 'fa-star-half-o';
      } else if (_inObject.summary >= 4) {
        starfour = 'fa-star';
      }
      if (_inObject.summary == 5) {
        starfive = 'fa-star';
      }

      let detailsTable = RateItCoolSettings.templates.reviews.details.html;
      let overview = RateItCoolSettings.templates.reviews.overview.html.split('$review.star5').join(_inObject.stars.five)
                      .split('$review.star4').join(_inObject.stars.four)
                      .split('$review.star3').join(_inObject.stars.three)
                      .split('$review.star2').join(_inObject.stars.two)
                      .split('$review.star1').join(_inObject.stars.one)
                      .split('$label.summaryReviewsTitle').join(RateItCoolSettings.labels.reviews.overview.summaryReviewsTitle);
      if (_inObject.details && _inObject.details.total && _inObject.details.total > 0) {
        overview = overview.split('$detailsTable').join(detailsTable)
                            .split('$details.total').join(_inObject.details.total)
                            .split('$review.total').join(_inObject.total)
                            .replace('$details.detail1.display', (_inObject.details == undefined || _inObject.details.detail1 == undefined ? 'display:none;':''))
                            .replace('$details.detail2.display', (_inObject.details == undefined || _inObject.details.detail2 == undefined ? 'display:none;':''))
                            .replace('$details.detail3.display', (_inObject.details == undefined || _inObject.details.detail3 == undefined ? 'display:none;':''))
                            .replace('$details.detail4.display', (_inObject.details == undefined || _inObject.details.detail4 == undefined ? 'display:none;':''))
                            .replace('$details.detail1.title', (_inObject.labels.detail1 ? _inObject.labels.detail1:''))
                            .replace('$details.detail2.title', (_inObject.labels.detail2 ? _inObject.labels.detail2:''))
                            .replace('$details.detail3.title', (_inObject.labels.detail3 ? _inObject.labels.detail3:''))
                            .replace('$details.detail4.title', (_inObject.labels.detail4 ? _inObject.labels.detail4:''))
                            .replace('$review.details.detail1', RateItCoolSettings.templates.reviews.stars.starsRender(_inObject.details.detail1, RateItCoolSettings.templates.reviews.detailsStars.html))
                            .replace('$review.details.detail2', RateItCoolSettings.templates.reviews.stars.starsRender(_inObject.details.detail2, RateItCoolSettings.templates.reviews.detailsStars.html))
                            .replace('$review.details.detail3', RateItCoolSettings.templates.reviews.stars.starsRender(_inObject.details.detail3, RateItCoolSettings.templates.reviews.detailsStars.html))
                            .replace('$review.details.detail4', RateItCoolSettings.templates.reviews.stars.starsRender(_inObject.details.detail4, RateItCoolSettings.templates.reviews.detailsStars.html))
                            .split('$label.detailTitle').join(RateItCoolSettings.labels.reviews.details.detailTitle);
      } else {
        overview = overview.split('$detailsTable').join('');
      }

      return RateItCoolSettings.templates.reviews.stars.html.split('$review.summary').join(Number((_inObject.summary).toFixed(2)))
                .split('$starone').join(starone)
                .split('$startwo').join(startwo)
                .split('$starthree').join(starthree)
                .split('$starfour').join(starfour)
                .split('$starfive').join(starfive)
                .split('$review.total').join(_inObject.total)
                .split('$label.review').join(RateItCoolSettings.labels.reviews.stars.review) + overview;
    },
    afterRender: _jQuery => {
      _jQuery('.rateit-cool-show-stars').on('click', e => {
        e.preventDefault();
        _jQuery('.rateit-cool-stars-detail-table').toggle('bounce');
      });
    }
  },
      form: {
    html: '<div class="rateit-cool-reviewform"><h3>$labels.reviewFormTitle</h3><form name="productDetailReviewform$gpnvalue"> <input type="hidden" name="gpntype" value="$gpntype"/> <input type="hidden" name="gpnvalue" value="$gpnvalue"/> <input type="hidden" name="language" value="$language"/> <table> <tr class="overall"> <td class="label second">$labels.reviewStarsTitle</td> <td> <span class="reviewStars"> <input type="hidden" name="stars" value="0"/> <span class="fa fa-star-o value1" title="$labels.oneStarTitle"></span> <span class="fa fa-star-o value2" title="$labels.twoStarTitle"></span> <span class="fa fa-star-o value3" title="$labels.threeStarTitle"></span> <span class="fa fa-star-o value4" title="$labels.fourStarTitle"></span> <span class="fa fa-star-o value5" title="$labels.fiveStarTitle"></span> </span> </td> </tr> <tr class="reviewDetail1" style="$display.detail1"> <td class="label">$details.detail1</td> <td> <span class="reviewStars"> <input type="hidden" name="stars" value="0"/> <span class="fa fa-star-o value1" title="$labels.oneStarTitle"></span> <span class="fa fa-star-o value2" title="$labels.twoStarTitle"></span> <span class="fa fa-star-o value3" title="$labels.threeStarTitle"></span> <span class="fa fa-star-o value4" title="$labels.fourStarTitle"></span> <span class="fa fa-star-o value5" title="$labels.fiveStarTitle"></span> </span> </td> </tr> <tr class="reviewDetail2" style="$display.detail2"> <td class="label second">$details.detail2</td> <td> <span class="reviewStars"> <input type="hidden" name="stars" value="0"/> <span class="fa fa-star-o value1" title="$labels.oneStarTitle"></span> <span class="fa fa-star-o value2" title="$labels.twoStarTitle"></span> <span class="fa fa-star-o value3" title="$labels.threeStarTitle"></span> <span class="fa fa-star-o value4" title="$labels.fourStarTitle"></span> <span class="fa fa-star-o value5" title="$labels.fiveStarTitle"></span> </span> </td> </tr> <tr class="reviewDetail3" style="$display.detail3"> <td class="label">$details.detail3</td> <td> <span class="reviewStars"> <input type="hidden" name="stars" value="0"/> <span class="fa fa-star-o value1" title="$labels.oneStarTitle"></span> <span class="fa fa-star-o value2" title="$labels.twoStarTitle"></span> <span class="fa fa-star-o value3" title="$labels.threeStarTitle"></span> <span class="fa fa-star-o value4" title="$labels.fourStarTitle"></span> <span class="fa fa-star-o value5" title="$labels.fiveStarTitle"></span> </span> </td> </tr> <tr class="reviewDetail4" style="$display.detail4"> <td class="label second">$details.detail4</td> <td> <span class="reviewStars"> <input type="hidden" name="stars" value="0"/> <span class="fa fa-star-o value1" title="$labels.oneStarTitle"></span> <span class="fa fa-star-o value2" title="$labels.twoStarTitle"></span> <span class="fa fa-star-o value3" title="$labels.threeStarTitle"></span> <span class="fa fa-star-o value4" title="$labels.fourStarTitle"></span> <span class="fa fa-star-o value5" title="$labels.fiveStarTitle"></span> </span> </td> </tr> </table> <br/> <div class="rateit-cool-review-title"> <input type="text" name="reviewTitle" placeholder="$placeholder.title" /> </div> <div class="rateit-cool-review-content"> <textarea name="reviewContent" placeholder="$placeholder.content"></textarea> </div> <div class="rateit-cool-review-recommend"> <input name="recommend" type="checkbox"> $labels.recommend </div> <br/> <div class="rateit-cool-review-security" style="$display.securitytext"> $labels.security.title <b>$securityText</b> <br/> <input name="securityText" type="text" placeholder=""> </div> <div class="rateit-cool-send-review"> <a href="#" data-formname="productDetailReviewform$gpnvalue" class="btn rateit-cool-send-produkt-review">$labels.submit</a><div class="clearfix"></div> </div> </form> <div class="rateit-cool-send-review-success" style="display:none"> $labels.successtext </div> <div class="rateit-cool-send-review-error" style="display:none"> $labels.errortext </div> </div>',
    render: _inObject => {
      if (RateItCoolSettings.publicReviews) {
        return RateItCoolSettings.templates.reviews.form.html
                .split('$gpntype').join(_inObject.gpntype)
                .split('$gpnvalue').join(_inObject.gpnvalue)
                .split('$language').join(_inObject.language)
                .split('$labels.oneStarTitle').join(RateItCoolSettings.labels.reviews.form.oneStarTitle)
                .split('$labels.twoStarTitle').join(RateItCoolSettings.labels.reviews.form.twoStarTitle)
                .split('$labels.threeStarTitle').join(RateItCoolSettings.labels.reviews.form.threeStarTitle)
                .split('$labels.fourStarTitle').join(RateItCoolSettings.labels.reviews.form.fourStarTitle)
                .split('$labels.fiveStarTitle').join(RateItCoolSettings.labels.reviews.form.fiveStarTitle)
                .replace('$display.detail1', (_inObject.overview.labels.detail1?'':'display:none'))
                .replace('$display.detail2', (_inObject.overview.labels.detail2?'':'display:none'))
                .replace('$display.detail3', (_inObject.overview.labels.detail3?'':'display:none'))
                .replace('$display.detail4', (_inObject.overview.labels.detail4?'':'display:none'))
                .replace('$details.detail1', (_inObject.overview.labels.detail1?_inObject.overview.labels.detail1:''))
                .replace('$details.detail2', (_inObject.overview.labels.detail2?_inObject.overview.labels.detail2:''))
                .replace('$details.detail3', (_inObject.overview.labels.detail3?_inObject.overview.labels.detail3:''))
                .replace('$details.detail4', (_inObject.overview.labels.detail4?_inObject.overview.labels.detail4:''))
                .split('$labels.recommend').join(RateItCoolSettings.labels.reviews.form.recommend)
                .split('$placeholder.title').join(RateItCoolSettings.labels.reviews.form.placeholderTitle)
                .split('$placeholder.content').join(RateItCoolSettings.labels.reviews.form.placeholderContent)
                .replace('$securityText', (RateItCoolSettings.securityText !== ''?RateItCoolSettings.securityText:''))
                .replace('$display.securitytext', (RateItCoolSettings.useSecurityText?'':'display:none'))
                .split('$labels.security.title').join(RateItCoolSettings.labels.reviews.form.securityTitle)
                .split('$labels.submit').join(RateItCoolSettings.labels.reviews.form.submit)
                .split('$labels.successtext').join(RateItCoolSettings.labels.reviews.form.successtext)
                .split('$labels.errortext').join(RateItCoolSettings.labels.reviews.form.errortext)
                .split('$labels.reviewFormTitle').join(RateItCoolSettings.labels.reviews.form.reviewFormTitle)
                .split('$labels.reviewStarsTitle').join(RateItCoolSettings.labels.reviews.form.reviewStarsTitle);
        } else {
          return '';
        }
    },
    afterRender: _jQuery => {

      _jQuery('.reviewStars span.value1').on('click', function(e) {
        e.preventDefault();
        _jQuery(this).parent().find('.fa-star').removeClass('fa-star').addClass('fa-star-o');
        _jQuery(this).removeClass('fa-star-o').addClass('fa-star');
        _jQuery(this).parent().find('input[name=stars]').val(1);
        _jQuery(this).parent().find('.star-text').html(_jQuery(this).attr('title'));
      });

      _jQuery('.reviewStars span.value2').on('click', function(e) {
        e.preventDefault();
        _jQuery(this).parent().find('.fa-star').removeClass('fa-star').addClass('fa-star-o');
        _jQuery(this).parent().find('.value1').removeClass('fa-star-o').addClass('fa-star');
        _jQuery(this).removeClass('fa-star-o').addClass('fa-star');
        _jQuery(this).parent().find('input[name=stars]').val(2);
        _jQuery(this).parent().find('.star-text').html(_jQuery(this).attr('title'));
      });

      _jQuery('.reviewStars span.value3').on('click', function(e) {
        e.preventDefault();
        _jQuery(this).parent().find('.fa-star').removeClass('fa-star').addClass('fa-star-o');
        _jQuery(this).parent().find('.value1').removeClass('fa-star-o').addClass('fa-star');
        _jQuery(this).parent().find('.value2').removeClass('fa-star-o').addClass('fa-star');
        _jQuery(this).removeClass('fa-star-o').addClass('fa-star');
        _jQuery(this).parent().find('input[name=stars]').val(3);
        _jQuery(this).parent().find('.star-text').html(_jQuery(this).attr('title'));
      });

      _jQuery('.reviewStars span.value4').on('click', function(e) {
        e.preventDefault();
        _jQuery(this).parent().find('.fa-star').removeClass('fa-star').addClass('fa-star-o');
        _jQuery(this).parent().find('.value1').removeClass('fa-star-o').addClass('fa-star');
        _jQuery(this).parent().find('.value2').removeClass('fa-star-o').addClass('fa-star');
        _jQuery(this).parent().find('.value3').removeClass('fa-star-o').addClass('fa-star');
        _jQuery(this).removeClass('fa-star-o').addClass('fa-star');
        _jQuery(this).parent().find('input[name=stars]').val(4);
        _jQuery(this).parent().find('.star-text').html(_jQuery(this).attr('title'));
      });

      _jQuery('.reviewStars span.value5').on('click', function(e) {
        e.preventDefault();
        _jQuery(this).parent().find('.fa-star').removeClass('fa-star').addClass('fa-star-o');
        _jQuery(this).parent().find('.value1').removeClass('fa-star-o').addClass('fa-star');
        _jQuery(this).parent().find('.value2').removeClass('fa-star-o').addClass('fa-star');
        _jQuery(this).parent().find('.value3').removeClass('fa-star-o').addClass('fa-star');
        _jQuery(this).parent().find('.value4').removeClass('fa-star-o').addClass('fa-star');
        _jQuery(this).removeClass('fa-star-o').addClass('fa-star');
        _jQuery(this).parent().find('input[name=stars]').val(5);
        _jQuery(this).parent().find('.star-text').html(_jQuery(this).attr('title'));
      });

      _jQuery('.rateit-cool-review-element .positive span.fa').on('click', function(e) {
        e.preventDefault();
        let element = _jQuery(this).parent();
        let type = 'positive';
        RateItCoolApi.put({
          type: type,
          gpntype: element.attr('data-gpntype'),
          gpnvalue: element.attr('data-gpnvalue'),
          language: element.attr('data-language'),
          id: element.attr('data-feedbackid')
        }, response => {
          if (response.success && response.success === 'true') {
            element.attr('data-positive', parseInt(element.attr('data-positive')) +1);
            element.find('.positiveValue').html(element.attr('data-positive'));
          }
        });
      });

      _jQuery('.rateit-cool-review-element .negative span.fa').on('click', function(e) {
        e.preventDefault();
        let element = _jQuery(this).parent();
        let type = 'negative';
        RateItCoolApi.put({
          type: type,
          gpntype: element.attr('data-gpntype'),
          gpnvalue: element.attr('data-gpnvalue'),
          language: element.attr('data-language'),
          id: element.attr('data-feedbackid')
        }, response => {
          if (response.success && response.success === 'true') {
            element.attr('data-negative', parseInt(element.attr('data-negative')) +1);
            element.find('.negativeValue').html(element.attr('data-negative'));
          }
        });

      });

      _jQuery('.rateit-cool-review-element .report span.fa').on('click', function(e) {
        e.preventDefault();
        let element = _jQuery(this).parent();
        let type = 'report';
        RateItCoolApi.put({
          type: type,
          gpntype: element.attr('data-gpntype'),
          gpnvalue: element.attr('data-gpnvalue'),
          language: element.attr('data-language'),
          id: element.attr('data-feedbackid')
        }, () => {

        });

      });

      _jQuery('.rateit-cool-send-review a').on('click', e => {
        e.preventDefault();
        let _form = _jQuery('.rateit-cool-reviewform form');
        // get data
        let data = {
          title: _form.find('input[name=reviewTitle]').val(),
          content: _form.find('textarea[name=reviewContent]').val(),
          stars: _form.find('.overall .reviewStars input[name=stars]').val(),
          details: {

          },
          recommend: 0
        };
        let _detail = _form.find('.reviewDetail1');
        let _detailsExists = false;
        if (_detail && _detail.attr('style') == '') {
          data.details.detail1 = _detail.find('.reviewStars input[name=stars]').val();
          _detailsExists = true;
        }
        _detail = _form.find('.reviewDetail2');
        if (_detail && _detail.attr('style') == '') {
          data.details.detail2 = _detail.find('.reviewStars input[name=stars]').val();
          _detailsExists = true;
        }
        _detail = _form.find('.reviewDetail3');
        if (_detail && _detail.attr('style') == '') {
          data.details.detail3 = _detail.find('.reviewStars input[name=stars]').val();
          _detailsExists = true;
        }
        _detail = _form.find('.reviewDetail4');
        if (_detail && _detail.attr('style') == '') {
          data.details.detail4 = _detail.find('.reviewStars input[name=stars]').val();
          _detailsExists = true;
        }
        if (_detailsExists) {
          data.stars = 0;
        }
        let _secTemp = _form.find('input[name=securityText]');
        let _isValid = false;
        if (RateItCoolSettings.useSecurityText &&  _secTemp && RateItCoolSettings.securityText == _secTemp.val()) {
          _isValid = true;
        } else if (!RateItCoolSettings.useSecurityText) {
          _isValid = true;
        }
        if(_form.find('input[name=recommend]:checked').length > 0) {
          data.recommend = 1;
        }
        if (_isValid) {
          RateItCoolApi.post(data, response => {
            if (response.success && response.success === 'true') {
              _form.hide();
              _form.parent().find('.rateit-cool-send-review-error').hide();
              _form.parent().find('.rateit-cool-send-review-success').show();
            } else {
              _form.parent().find('.rateit-cool-send-review-success').hide();
              _form.parent().find('.rateit-cool-send-review-error').show();
            }
          });
        } else {
          // mark all as red
          if (RateItCoolSettings.useSecurityText && RateItCoolSettings.securityText !== _secTemp.val()) {
            _secTemp.addClass('error');
          }
          if (data.title == '') {
            _form.find('input[name=reviewTitle]').addClass('error');
          }
          if (data.content == '') {
            _form.find('textarea[name=reviewContent]').addClass('error');
          }
        }
      });
    }
  },
      missing: {
    html: '<div class="rateit-cool-missing-reviews-elements">$labels.missingtitle<hr/>$reviewForm</div>',
    render: _inObject => {
      return RateItCoolSettings.templates.reviews.missing.html
                  .replace('$labels.missingtitle',RateItCoolSettings.labels.reviews.missing.missingTitle)

      ;
    }
}
    }
  }
};
