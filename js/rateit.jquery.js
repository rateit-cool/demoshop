const RateItCoolApi = (function(){
  "use strict";
  
  const _customSettings = RateItCoolSettings;
  const _version = '1.0.0';
  const _assetsURL = 'https://assets.rateit.cool';
  const _server = 'https://api.rateit.cool/';

  let _noConflict = false;
  let _jQuery = null;
  let _oldjQuery = null;
  let _labels = {};
  let _jsLoaded = false;
  let _extraParameters = {skip: 0};

  const _zerofilledGtin = (n,w) => {
    if (n.length < 13) {
      const n_ = Math.abs(n);
      const zeros = Math.max(0, w - Math.floor(n_).toString().length );
      const zeroString = Math.pow(10,zeros).toString().substr(1);
      return zeroString+n;
    }
    return n;
  }

  const _inject = (element, type, src, callback) => {
    const jqTag = document.createElement(element);
    jqTag.type = type;
    if (src) {
      if (element === 'script') {
        jqTag.src = src;
      } else if (element === 'link') {
        jqTag.href = src;
        jqTag.rel = 'stylesheet';
      }
    }
    if (callback) {
      jqTag.onload = callback;
    }
    document.getElementsByTagName('head')[0].appendChild(jqTag);
  };

  /**
   * prepare the page / load jquery if needed
   */
  const _prepare = callback => {
    if (!_jsLoaded) {
      _jsLoaded = true;
      if (typeof jQuery === 'undefined' || ( parseInt(jQuery.fn.jquery.split('.')[0]) <= 1 && parseInt(jQuery.fn.jquery.split('.')[1]) < 7)) {
        _noConflict = true;
        _inject('script', 'text/javascript', _assetsURL + '/js/jquery-2.1.4.min.js', callback);
      } else {
        // jQuery exists set as own
        _jQuery = jQuery;
      	callback();
      }
    } else {
      callback();
    }
  };

  const _ajax = (method, type, gpntype, values, language, extraParameter, successCallback, errorCallback) => {
    if (!language) {
      language = _customSettings.language;
    }
    let _url = _server + type + '/' + gpntype + '/' + values + '/' + language;
    if (method.toLowerCase() !== 'post') {
      _url += '?' + extraParameter;
    }
    _jQuery.ajax({
        url : _url,
        method: method.toUpperCase(),
        data: (method === 'post'?extraParameter:null),
        dataType : 'json',
        crossDomain: true,
        async: false,
        beforeSend: function(xhr) {
          xhr.setRequestHeader("Authorization", "Basic " + btoa(_customSettings.username + ":" + _customSettings.apikey));
          xhr.setRequestHeader("X-Api-Version", _version);
        },
        success: data => {
          successCallback(data);
        },
        error: () => {

        }
      });
  };

  const _stars = () => {
    let allElements = [];
    let gpnvalues = {};
    let gpnvalue = "";
    let language = null;
    const template = _customSettings.templates.stars.html;

    _jQuery('.rateit-cool-product').each(function() {
      if (!_jQuery(this).attr('data-loaded')) {
        let gpntype = _jQuery(this).attr('data-gpntype');
        if (!gpntype) {
          gpntype = _customSettings.username;
        }
        language = (_jQuery(this).attr('data-language')?_jQuery(this).attr('data-language'):_customSettings.language);
        gpnvalue = _jQuery(this).attr('data-gpnvalue');
        if (gpnvalue) {
          if (gpnvalues[gpntype] == undefined) {
            gpnvalues[gpntype] = [];
          }
          gpnvalues[gpntype].push( gpnvalue );
        }
      }
    });

    _jQuery.each(gpnvalues, (gpntype, values) => {
      _ajax('get','stars', gpntype, values.join(','), language, '&limit=' + _customSettings.limit, 
          data => {
            if (data.length > 0) {
              data.forEach((oneRatingResponse) => {
                oneRatingResponse.language = language;
                let destinationElement = _jQuery('.rateit-cool-product[data-gpnvalue="' + oneRatingResponse.gpnvalue + '"]');
                destinationElement.attr('data-loaded','true');
                destinationElement.html(_customSettings.templates.stars.render(oneRatingResponse));
              });
            }
          }, () => {

          });
    });
  }

  const _transformParameters = () => {
    let rString = [];
    for (let key in _extraParameters) {
      if (_extraParameters.hasOwnProperty(key)) {
        rString.push(key + '=' + _extraParameters[key]);
      }
    }
    return rString.join('&');
  }

  const _reviewsPost = (data, callback) => {
    const reviewElement = _jQuery('.rateit-cool-product-reviews');
    const gpntype = reviewElement.attr('data-gpntype');
    const gpnvalue = reviewElement.attr('data-gpnvalue');
    const language = (reviewElement.attr('data-language')?reviewElement.attr('data-language'):_customSettings.language);

    _ajax('post','feedback', gpntype, gpnvalue, language, JSON.stringify(data),
          response => {
            callback(response);
          }, 
          () => {
            callback({success: 'false'});
          });
  }

  const _reviewsPut = (data, callback) => {
    const gpntype = data.gpntype;
    const gpnvalue = data.gpnvalue;
    const language = (data.language?data.language:_customSettings.language);
    let extraParameter = 'id=' + data.id;

    if (data.type) {
      if (data.type === 'positive' || data.type === 'negative') {
        extraParameter += '&helpful=' + data.type;
      } else if (data.type === 'incorrect') {
        extraParameter += '&incorrect=1';
      }
    }

    _ajax('put','feedback', gpntype, gpnvalue, language, extraParameter, 
        response => {
          callback(response);
        }, () => {
          callback({success: 'false'});
        });
  }

  const _reviews = (parameters, callback) => {
    const reviewElement = _jQuery('.rateit-cool-product-reviews');
    if (reviewElement.length > 0) {
      const gpntype = reviewElement.attr('data-gpntype');
      const gpnvalue = reviewElement.attr('data-gpnvalue');
      const language = (reviewElement.attr('data-language')?reviewElement.attr('data-language'):_customSettings.language);
      let extraParameter = reviewElement.attr('data-extraParameter');

      if (!extraParameter) {
        reviewElement.attr('data-extraParameter','');
        extraParameter = '';
      }

      if (parameters.sortField && parameters.sort) {
        _extraParameters.sort = parameters.sort;
        _extraParameters.sortField = parameters.sortField;
        _extraParameters.skip = 0;
      } else if (parameters.stars) {
        _extraParameters.skip = 0;
        _extraParameters.stars = parameters.stars;
      }

      if (parameters.skip && parameters.skip == 0) {
        _extraParameters.skip = 0;
      }

      if (parameters.skip && parameters.skip > 0) {
        _extraParameters.skip += parameters.skip;
      }

      extraParameter = _transformParameters();
      reviewElement.attr('data-extraParameter',extraParameter);

      _ajax('get','feedback', gpntype, gpnvalue, language, extraParameter += '&limit=' + _customSettings.limit, 
        data => {
          data.language = language;
          if (!parameters.refresh && !parameters.addreviews) {
            const starDestinationElement = _jQuery('.rateit-cool-product-detail');
            starDestinationElement.html(_customSettings.templates.reviews.stars.render(data.overview));
            // add some events
            _customSettings.templates.reviews.stars.afterRender(_jQuery);
          }
          const reviewDestinationElement = _jQuery('.rateit-cool-product-review-list');
          let reviewList = '';
          if (!parameters.addreviews) {
            let reviewForm = _customSettings.templates.reviews.form.render(data);
            if (data.elements && data.elements.length > 0) {
              // element around the eleemnts
              let reviewElement = _customSettings.templates.reviews.list.render(data);
              data.elements.forEach(oneReview => {
                oneReview.gpntype = gpntype;
                oneReview.gpnvalue = gpnvalue;
                oneReview.language = language;
                reviewList += _customSettings.templates.reviews.element.render(oneReview, data.overview.labels);
              });
              reviewList = reviewElement.split('$list').join(reviewList);
            } else {
              // render missing feedbacks
              reviewList = _customSettings.templates.reviews.missing.render(data);
            }

            reviewDestinationElement.html(reviewList.replace('$reviewForm', reviewForm));
            _customSettings.templates.reviews.form.afterRender(_jQuery);

            _customSettings.templates.reviews.list.afterRender(_jQuery);
          } else if (data.elements && data.elements.length > 0) {
            let commentList = reviewDestinationElement.find('#comment-list');
            data.elements.forEach(oneReview => {
              commentList.append(_customSettings.templates.reviews.element.render(oneReview, data.overview.labels));
            });
          }

        if (callback) {
          callback(data);
        }
      });
    }

  }

  return {
    stars: () => {
      _stars();
    },
    reviews: (parameters, callback) => {
      _reviews(parameters, callback);
    },
    parameters: () => {
      return _extraParameters;
    },
    post: (data, callback) => {
      _reviewsPost(data, callback);
    },
    put: (data, callback) => {
      _reviewsPut(data, callback);
    },
    init: () => {
      _prepare(() => {
        if (_noConflict) {
          _oldjQuery = $;
          _jQuery = jQuery.noConflict();
          $ = _oldjQuery;
          jQuery = $;
        } else {
          _jQuery = $;
        }
        if (_customSettings.defaultcss) {
          _inject('link','text/css', _assetsURL + '/css/font-awesome.min.css');
    		  if (_customSettings.apikey !== 'YOURAPIKEY') {
    			  _inject('link','text/css', _assetsURL + '/css/' + _customSettings.apikey + '/' + _customSettings.apikey + '.css');
    		  }
        }
        
        // request stars
        _stars();
        
        const userAgent = new UserAgent().parse(navigator.userAgent);
        if (!userAgent.isBot) {
          _reviews({refresh: false, addreviews: false}, null);
        }
      });
    }
  }
})();

// init our api
RateItCoolApi.init();
