# demoshop
A shop implementation to demonstrate the integration of the rateit.cool service for developer.

## preparation
1. Create an account at rateit.cool (https://app.rateit.cool/register/en)
2. Click at the left menu on the element "Shop"
3. Copy the username, apikey and serverapikey to a file-editor

## the service it self

### server calls
The server calls are only for the integration of your own reviews for the search engines and indexes. 

### frontend ( browser ) calls
The stars and reviews will integrated via AJAX calls on the fly. The review sending form will also send its content to the service via AJAX and POST message.

## verifying your shop
Add the `meta` tag at the top of your website / shop. You will find the ready to use `meta` tag in the backend of rateit.cool in the shop section.

Here is a litte instruction to get the `meta` tag value:
0. Login with your account at rateit.cool (https://app.rateit.cool/login/en)
0. Click at the left menu on the element "Shop"
0. Copy the meta-tag from the Shop Verifing (Meta-Tag) section
0. Paste it to your HTML-File

## frontend integration

### integration javascript
Add the javascript near the `</body>` tag.

```
<script src="js/express-useragent.min.js"></script>
<script src="js/customized.jquery.js"></script>
<script src="js/rateit.jquery.js"></script>
```

### show stars at any list
To show the stars directly to the productname, you have only integrate some data elements.
```
<div class="rateit-cool-product" data-gpntype="gtin" data-gpnvalue="GTIN-NUMBER"></div>
```

### show reviews and stars at the detail page
To show the stars directly to the productname, you have only integrate some data elements and one div for the reviews.
```
<div class="rateit-cool-product-reviews" data-gpntype="gtin" data-gpnvalue="GTIN-NUMBER" data-language="en-US">Reviewslabel or some other text</div>
<div class="rateit-cool-product-review-list">The reviews itself.</div>
```

If you want to show the stars directly at the productname on the product detail page, you have to add the fellowing element to the productname
```
PRODUCTNAME
<div class="rateit-cool-product-detail"></div>
```

### customize the layout and handling
If you want to customize the layout and / or the handling, you can change everything in the customized.jquery.js.

## serverside integration

### request own reviews
To request your own reviews for the search engines you have to integrate the fellowing
```
require_once('lib/RateItCoolApi.php');
$rateItCoolApi = new RateItCoolApi(array(
  'username' => 'YOUR-USERNAME',
  'serverapikey' => 'YOUR-SERVER-APIKEY',
  'apikey' => 'YOUR-APIKEY'
));
```

To show the reviews for the search engines, only integrate the example in your HTML
```
<div class="rateit-cool-product-review-list">
<?php
	if (isset($_yourOwnReviews['elements'])) {
	$rateitcool_div = '';
		foreach($_yourOwnReviews['elements'] as $_review) {
			$rateitcool_div .= '<div itemscope itemtype="http://schema.org/Review">';
			$timeObj = new DateTime($_review['time']);
			$rateitcool_div .= '<meta itemprop="datePublished" content="' . $timeObj->format('d-m-YYYY') . '">' . $timeObj->format('d-m-YYYY');
			$rateitcool_div .= '<div itemprop="itemReviewed" itemscope itemtype="http://schema.org/Product">';
			$rateitcool_div .= '<span itemprop="name">' . $eans[$gtin] . '</span>';
			$rateitcool_div .= '</div>';
			$rateitcool_div .= '<span itemprop="reviewRating" itemscope itemtype="http://schema.org/Rating">';
			$rateitcool_div .= '<span itemprop="ratingValue">' . $_review['stars'] . '</span>';
			$rateitcool_div .= '<br/>Maximum Stars <span itemprop="bestRating">5</span>';
			$rateitcool_div .= '<br/>Minimum Stars <span itemprop="worstRating">1</span>';
			$rateitcool_div .= '</span>';
			$rateitcool_div .= '<br/>';
			$rateitcool_div .= '<h3><span itemprop="name">' . $_review['title'] . '</span></h3>';
			$rateitcool_div .= '<span itemprop="reviewBody">' . $_review['content'] . '</span>';
			$rateitcool_div .= '<div itemprop="publisher" itemscope itemtype="http://schema.org/Organization">';
			$rateitcool_div .= '<meta itemprop="name" content="">';
			$rateitcool_div .= '</div>';
			$rateitcool_div .= '</div>';
			$rateitcool_div .= '<hr/>';
		}
		echo $rateitcool_div;
	}
?>
</div>
```
The content in the div will removed from the ajax call of the frontend integration.

# test
We implemented a frontend test with puppeteer (https://github.com/GoogleChrome/puppeteer) to test the implementation of the ajax calls as a bot. The main goal is, that googleBot don't indexing the reviews that do not have the source of this shop.

You can find the files in the directory "test".

## how to use it
1. run `yarn` or `npm install` in the directory test
2. run `yarn test` or `npm test`

# contact
If you have any question or some feedback to this demoshop, please conatct us:
* email: info@rateit.cool
* twitter: https://www.twitter.com/RateItCool
