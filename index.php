<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Demo-Shop rateit.cool</title>

    <link rel="dns-prefetch" href="//maxcdn.bootstrapcdn.com">
    <link rel="dns-prefetch" href="//oss.maxcdn.com">
    <link rel="dns-prefetch" href="//ajax.googleapis.com">
    <link rel="dns-prefetch" href="//api.rateit.cool">
    <link rel="dns-prefetch" href="//assets.rateit.cool">
    <link rel="preconnect" href="https://assets.rateit.cool">
    <link rel="preconnect" href="https://maxcdn.bootstrapcdn.com">
    <link rel="preconnect" href="https://oss.maxcdn.com">
    <link rel="preconnect" href="https://ajax.googleapis.com">
    <link rel="preconnect" href="https://api.rateit.cool">
    
    <!-- Bootstrap Core CSS -->
    <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css" rel="stylesheet">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css">

    <!-- Custom CSS -->
    <link href="css/shop-homepage.css" rel="stylesheet">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
</head>
<body>
    <!-- Page Content -->
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <p class="lead">Demo Shop rateit.cool</p>
            </div>
            <div class="col-md-12">
                <div class="row carousel-holder">
                    <div class="col-md-12">
                        <div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
                            <ol class="carousel-indicators">
                                <li data-target="#carousel-example-generic" data-slide-to="0" class="active"></li>
                                <li data-target="#carousel-example-generic" data-slide-to="1"></li>
                                <li data-target="#carousel-example-generic" data-slide-to="2"></li>
                            </ol>
                            <div class="carousel-inner">
                                <div class="item active">
                                    <img class="slide-image" src="images/first.jpeg" alt="">
                                </div>
                                <div class="item">
                                    <img class="slide-image" src="images/second.jpeg" alt="">
                                </div>
                                <div class="item">
                                    <img class="slide-image" src="images/third.jpeg" alt="">
                                </div>
                            </div>
                            <a class="carousel-control left" href="#carousel-example-generic" data-slide="prev">
                                <span class="fa fa-chevron-left"></span>
                            </a>
                            <a class="carousel-control right" href="#carousel-example-generic" data-slide="next">
                                <span class="fa fa-chevron-right"></span>
                            </a>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-4 col-lg-4 col-md-4">
                        <div class="thumbnail">
                            <img src="images/5099206016101.png" alt="">
                            <div class="caption">
                                <h4 class="pull-right">$xx.xx</h4>
                                <h4><a href="detail.php?gtin=5099206016101">LOGITECH Performance<br/>Mouse MX Wireless Black<br/>910-001120</a>
                                </h4>
                            </div>
                            <div class="rateit-cool-product" data-gpntype="gtin" data-gpnvalue="5099206016101">
                            </div>
                        </div>
                    </div>

                    <div class="col-sm-4 col-lg-4 col-md-4">
                        <div class="thumbnail">
                            <img src="images/4011170080989.png" alt="">
                            <div class="caption">
                                <h4 class="pull-right">$xx.xx</h4>
                                <h4><a href="detail.php?gtin=4011170080989">REINERSCT cyberJack®<br>RFID basis</a>
                                </h4>
                            </div>
                            <div class="rateit-cool-product" data-gpntype="gtin" data-gpnvalue="4011170080989">
                            </div>
                        </div>
                    </div>

                    <div class="col-sm-4 col-lg-4 col-md-4">
                        <div class="thumbnail">
                            <img src="images/5060155408064.png" alt="">
                            <div class="caption">
                                <h4 class="pull-right">$xx.xx</h4>
                                <h4><a href="detail.php?gtin=5060155408064">IROBOT Roomba 880</a></h4>
                            </div>
                            <div class="rateit-cool-product" data-gpntype="gtin" data-gpnvalue="5060155408064">
                            </div>
                        </div>
                    </div>

                    <div class="col-sm-4 col-lg-4 col-md-4">
                        <div class="thumbnail">
                            <img src="images/4210201096627.png" alt="">
                            <div class="caption">
                                <h4 class="pull-right">$xx.xx</h4>
                                <h4>
                                  <a href="detail.php?gtin=4210201096627">ORAL-B Pro 2000</a>
                                </h4>
                            </div>
                            <div class="rateit-cool-product" data-gpntype="gtin" data-gpnvalue="4210201096627">
                            </div>
                        </div>
                    </div>

                    <div class="col-sm-4 col-lg-4 col-md-4">
                        <div class="thumbnail">
                            <img src="images/4210201109150.jpg" alt="">
                            <div class="caption">
                                <h4 class="pull-right">$xx.xx</h4>
                                <h4>
                                  <a href="detail.php?gtin=4210201109150">Braun Silk-épil 9 9-961</a>
                                </h4>
                            </div>
                            <div class="rateit-cool-product" data-gpntype="gtin" data-gpnvalue="4210201109150">
                            </div>
                        </div>
                    </div>

                    <div class="col-sm-4 col-lg-4 col-md-4">
                        <h4><a href="#">Like this service?</a>
                        </h4>
                        <p>If you like this service, then <a target="_blank" href="https://app.rateit.cool/register/en/">create a free account</a> and use the rateit.cool service!</p>
                        <a class="btn btn-primary" target="_blank" href="https://app.rateit.cool/register/en">Register for free</a>
                    </div>

                </div>

            </div>

        </div>

    </div>
    <!-- /.container -->

    <div class="container">

        <hr>

        <!-- Footer -->
        <footer>
            <div class="row">
                <div class="col-lg-12">

                </div>
            </div>
        </footer>

    </div>
    <!-- /.container -->

    <!-- jQuery -->
    <script type="text/javascript" src="js/express-useragent.min.js"></script>
    <script type="text/javascript" src="js/customized.jquery.js"></script>
    <script type="text/javascript" src="js/rateit.jquery.js"></script>
    <!-- Bootstrap Core JavaScript -->
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js"></script>

</body>

</html>
